#include <jam2/initcond/Constraints.h>
#include <jam2/collision/Collision.h>
#include <cstdlib>

using namespace std;
using namespace Pythia8;

namespace jam2 {

Constraints::Constraints(Settings* s):settings(s)
{
  gWidth=settings->parm("MeanField:gaussWidth");
  cWidth=settings->parm("Cascade:gaussWidth");
  tStart = settings->parm("Cascade:timeStart");
  optGij=settings->mode("Cascade:optGij");
  optCollisionOrdering = settings->mode("Cascade:optCollisionOrder");
}

double Constraints::lambda(const Vec4& p, const Vec4& phat,double m, double lambda)
{
  if(optCollisionOrdering < 100) {
    return 1.0/p.e();
  } else if(optCollisionOrdering ==101) {
    return 1.0/(p*phat);
  } else if(optCollisionOrdering < 110) {
    if(!m) {
      cout << "Constraints::lambda m= "<< m <<endl;
      exit(1);
    }
    return 1.0/m;
  } else if(optCollisionOrdering == 110) {
    return settings->parm("Cascade:LagrangeMultiplier");
  } else if(optCollisionOrdering >= 120) {
    return lambda;
  } else {
    cout <<"Expanding::generage wrong option opt= "<< optCollisionOrdering <<endl;
    exit(1);
  }
}

double sign(double A) {
  return (A>0)-(A<0);
}

double Constraints::makeConstraintsMatrix(vector<Pythia8::Vec4>& r,vector<Pythia8::Vec4>& p)
{
  int NV=r.size();
  if(NV ==1) return 0.0;
  chi.assign(NV,0.0);
  chi1.assign(NV,0.0),chi2.assign(NV,0.0);
  mat.assign(NV,vector<double>(NV+1,0.0));
  static const double epsG=1e-9;

  Vec4 U=0.0;
  Vec4 R=0.0;
  for(int i=0;i<NV; i++) {
    Vec4 r1=r[i];
    Vec4 p1=p[i];
    U += p1;
    R += r1;
    for(int j=i+1; j<NV; j++) {
      Vec4 r2=r[j];
      Vec4 p2=p[j];
      Vec4 u12=p1+p2;
      u12 /= u12.mCalc();
      Vec4 r12=r1-r2;
      double ur12=u12*r12;
      double G12=1.0, dq0=u12[0];
      double q12 = r12*r12/(4*cWidth);
      if(q12>0.0) continue;
      double qt12 = q12 - ur12*ur12/(4*cWidth);
      double q0 = r12[0] - ur12*u12[0];

      if(optGij==1) {
        G12= exp(q12)/abs(q12-epsG);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12*(1.-sign(q12)/(q12-epsG))+u12[0]);
        //dq0=G12*(2*r12[0]/(4*cWidth)*ur12*(1.-1.0/q12)+u12[0]);
      } else if(optGij==2) {
        G12= exp(q12)/(q12-epsG);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12*(1.-1./(q12-epsG))+u12[0]);
      } else if(optGij==3) {
        G12= exp(q12);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12+u12[0]);
      } else if(optGij==4) {
        G12= exp(qt12)/qt12;
        dq0=G12*(2*q0/(4*cWidth)*ur12*(1.-1./qt12)+u12[0]);
      } else if(optGij==5) {
        G12= exp(qt12)/abs(qt12);
        dq0=G12*(2*q0/(4*cWidth)*ur12*(1.-1./qt12)+u12[0]);
      } else if(optGij==6) {
        G12= exp(qt12);
        dq0=G12*(2*q0/(4*cWidth)*ur12+u12[0]);

      } else if(optGij==12) {
        double rt=r12.pT();
        Vec4  qhat = r12/rt;
        qhat[0]=0.0;qhat[3]=0.0;
        G12= exp(q12)/q12;
        dq0=G12*(-2*rt/(4*cWidth)*ur12*(1.-1./q12)+u12*qhat);
      }

      if(isinf(dq0) || isnan(dq0)) {
	cout << " inf dq0 = "<< dq0 << " G12= "<< G12
	  << " u12= "<< u12[0]
	  <<endl;
	exit(1);
      }

      chi[i]  += G12*ur12;
      chi[j]  -= G12*ur12;

      // need for iteration method.
      chi1[i] += G12*u12[0];
      chi1[j] += G12*u12[0];
      chi2[i] += G12*(u12[0]*r2[0] + dot3(u12,r12));
      chi2[j] += G12*(u12[0]*r1[0] - dot3(u12,r12));

      // for Newton method.
      mat[i][i] += dq0;
      mat[j][j] += dq0;
      mat[i][j] = -dq0;
      mat[j][i] = -dq0;

      //mat[i][NV] += -G12*ur12;
      //mat[j][NV] +=  G12*ur12;

    }
  }
  U /= U.mCalc();
  chi[NV-1]=(U*R)/NV - tStart;
  mat[NV-1][NV]= -chi[NV-1];
  double eps=0.0;
  for(int i=0;i<NV;i++) {
    mat[NV-1][i]=U[0]/NV;
    mat[i][NV] = -chi[i];
    eps += abs(chi[i]);
  }
  return eps;
}

double Constraints::makeConstraints(vector<Pythia8::Vec4>& r,vector<Pythia8::Vec4>& p)
{
  double eps = makeConstraintsMatrix(r,p);
  if(eps < 1e-5) return eps;

  static const int opt=1;
  // fix time-constraints by iteration.
  if(opt==0) {
    for(int i=0;i<(int)r.size(); i++)
    if(chi1[i] !=0.0) r[i][0]=chi2[i]/chi1[i];
    return eps;
  }

  if(opt==1) w4UL(r);
  //if(opt==1) w4LU(r);
  else if(opt==2) newtonRaphson(r);

  return eps;

}

// Fix time by W4 UL method.
void Constraints::w4UL(vector<Pythia8::Vec4>& r)
{
  int NV=r.size();

  // UL decomposition.
  for(int k=NV-1;k>=1;k--) {
    double pivot = mat[k][k];
    int l=k;
    for(int j=k-1;j>=0;j--)
    if(abs(pivot) < abs(mat[j][k]) ) {
      l = j; pivot = mat[j][k];
    }
    if(l != k) {
      for(int j=0;j<=NV;j++) {
	swap(mat[k][j],mat[l][j]);
        swap(pW4[k],pW4[l]);
      }
    }
    // end of pivotting

    for(int j=k-1; j>=0;j--) mat[k][j] /= pivot;
    for(int i=k-1; i>=0;i--)
    for(int j=k-1; j>=0;j--)
      mat[i][j] -=  (mat[i][k] * mat[k][j]);
  }

  bool isW4=0;
  double dtau=1.0;
  vector<double> pW40=pW4;

  // W4UL method
  if(isW4) {

  // forward substitution.
  for(int k=0;k<NV;k++) {
    for(int j=0;j<k;j++) {
      pW40[k] -= mat[k][j] * pW40[j];
    }
  }

  // backward substitution. 
  for(int k=NV-1;k>=0;k--) {
    for(int j=k+1;j<NV;j++) {
      mat[k][NV] -= mat[k][j] * mat[j][NV];
    }
    mat[k][NV] /= mat[k][k];
  }

  for(int i=0;i<NV;i++) {
    r[i][0] += dtau*pW40[i];
    cout << scientific << " pw4= "<< pW40[i]<<endl;
    pW4[i] = dtau*mat[i][NV] + (1.-2*dtau)*pW4[i];
  }

  // Newton method
  } else {

  // backward substitution. 
  for(int k=NV-1;k>=0;k--) {
    for(int j=k+1;j<NV;j++) mat[k][NV] -=  mat[k][j] * mat[j][NV];
    mat[k][NV] /= mat[k][k];
  }
  // forward substitution.
  for(int k=0;k<NV;k++) {
    for(int j=0;j<k;j++) mat[k][NV] -= mat[k][j] * mat[j][NV];
  }
  for(int i=0;i<NV;i++) r[i][0] += dtau*mat[i][NV];

  }



}

// Fix time by W4 LU method.
void Constraints::w4LU(vector<Pythia8::Vec4>& r)
{
  int NV=r.size();

  // LU decomposition.
  for(int k=0;k<NV-1;k++) {
    double pivot = mat[k][k];
    int l=k;
    for(int j=k+1;j<NV;j++)
    if(abs(pivot) < abs(mat[j][k]) ) {
      l = j; pivot = mat[j][k];
    }
    if(l != k) {
      for(int j=0;j<=NV;j++) {
	swap(mat[k][j],mat[l][j]);
        swap(pW4[k],pW4[l]);
      }
    }
    // end of pivotting

    for(int j=k+1; j<NV;j++) mat[j][k] /= pivot;
    for(int i=k+1; i<NV;i++)
    for(int j=k+1; j<NV;j++)
      mat[i][j] -=  (mat[i][k] * mat[k][j]);
  }

  bool isW4=0;
  double dtau=1.0;
  vector<double> pW40=pW4;

  // W4LU method
  if(isW4) {

  // forward substitution.
  for(int k=0;k<NV;k++) {
    for(int j=0;j<k;j++) {
      pW40[k] -= mat[k][j] * pW40[j];
    }
  }

  // backward substitution. 
  for(int k=NV-1;k>=0;k--) {
    for(int j=k+1;j<NV;j++) {
      mat[k][NV] -= mat[k][j] * mat[j][NV];
    }
    mat[k][NV] /= mat[k][k];
  }

  for(int i=0;i<NV;i++) {
    r[i][0] += dtau*pW40[i];
    cout << " pw4= "<< pW40[i]<<endl;
    pW4[i] = dtau*mat[i][NV] + (1.-2*dtau)*pW4[i];
  }

  // Newton method
  } else {

  // forward substitution.
  for(int k=0;k<NV;k++) {
    for(int j=0;j<k;j++) mat[k][NV] -= mat[k][j] * mat[j][NV];
  }
  // backward substitution. 
  for(int k=NV-1;k>=0;k--) {
    for(int j=k+1;j<NV;j++) mat[k][NV] -=  mat[k][j] * mat[j][NV];
    mat[k][NV] /= mat[k][k];
  }
  for(int i=0;i<NV;i++) r[i][0] += dtau*mat[i][NV];

  }

}

void Constraints::newtonRaphson(vector<Pythia8::Vec4>& r)
{
  int NV=r.size();
  // fix time by Newton method.
  for (int k=0; k<NV; k++) {
    if (!mat[k][k]) {
      cout << "Constraints::makeConstraints singlular matirx "<< mat[k][k] << endl;
      exit(1);
    }
    for (int i=k+1; i<NV; i++) {
      double f = mat[i][k]/mat[k][k];
      if(isnan(f)) {
	cout << "nan? mat= "<< f << " mat[k][k]= "<< mat[k][k] << " matik= "<< mat[i][k] <<endl;
	exit(1);
      }
      for (int j=k+1; j<=NV; j++) mat[i][j] -= mat[k][j]*f;
      mat[i][k] = 0;
    }
  }

  double dtt=0.0;
  // Start calculating from last equation up to the first.
  for (int i = NV-1; i >= 0; i--) {
    for (int j=i+1; j<NV; j++) {
      mat[i][NV] -= mat[i][j]*mat[j][NV];
    }
    mat[i][NV] /= mat[i][i];
    dtt += abs(mat[i][NV]);
  }
  dtt /= NV;

  double alpha=1.0;
  if(dtt > 0.2) alpha = 0.2/dtt;
  cout << " dtt= "<< dtt << " alpha= "<< alpha << endl;

  for(int i=0;i<NV;i++) {
    if(optGij==12) {
       double rt=r[i].pT();
      r[i][1] += alpha * mat[i][NV]*r[i][1]/rt;
      r[i][2] += alpha * mat[i][NV]*r[i][2]/rt;
    } else {
      r[i][0] += alpha * mat[i][NV];
    }
  }

}

vector<double> Constraints::solveConstraints(vector<Pythia8::Vec4>& r,vector<Pythia8::Vec4>& p,double m)
{
  int NV=r.size();
  // two times the Lagrange multipliers.
  vector<double> lambda2(NV);
  if(NV ==1) {lambda2[0]=2.0/m; return lambda2;}

  chi.assign(NV,0.0);
  mat.assign(NV,vector<double>(NV+1,0.0));

  Vec4 U=0.0;
  Vec4 R=0.0;
  for(int i=0;i<NV; i++) {
    Vec4 r1=r[i];
    Vec4 p1=p[i];
    U += p1;
    R += r1;
    for(int j=i+1; j<NV; j++) {
      Vec4 r2=r[j];
      Vec4 p2=p[j];
      Vec4 u12=p1+p2;
      u12 /= u12.mCalc();
      Vec4 r12=r1-r2;
      double ur12=u12*r12;
      double G12=1.0;
      double dq1=2*p1*u12;
      double dq2=2*p2*u12;
      double q12 = r12*r12/(4*cWidth);
      //if(q12>0.0) continue;
      double qt12 = q12 - ur12*ur12/(4*cWidth);
      if(optGij==1) {
        G12= exp(q12)/abs(q12);
        dq1=G12*(2/(4*cWidth)*ur12*(1.-sign(q12)/q12)*2*(r12*p1) + 2*(p1*u12));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-sign(q12)/q12)*2*(r12*p2) + 2*(p2*u12));
      } else if(optGij==2) {
        G12= exp(q12)/q12;
        dq1=G12*(2/(4*cWidth)*ur12*(1.-1./q12)*2*(r12*p1) + 2*(p1*u12));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-1./q12)*2*(r12*p2) + 2*(p2*u12));
      } else if(optGij==3) {
        G12= exp(q12);
        dq1=G12*(2/(4*cWidth)*ur12*2*(r12*p1) + 2*(p1*u12));
        dq2=G12*(2/(4*cWidth)*ur12*2*(r12*p2) + 2*(p2*u12));

      } else if(optGij==4 || optGij==5) {
        G12= optGij==3 ? exp(qt12)/qt12 : exp(qt12)/abs(qt12);
        Vec4 q = r12 - ur12*u12;
        dq1=G12*(2/(4*cWidth)*ur12*(1.-1./qt12)*2*(q*p1) + 2*(p1*u12));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-1./qt12)*2*(q*p2) + 2*(p2*u12));
      } else if(optGij==6) {
        G12= exp(qt12);
        Vec4 q = r12 - ur12*u12;
        dq1=G12*(2/(4*cWidth)*ur12*2*(p1*q)+2*(p1*u12));
        dq2=G12*(2/(4*cWidth)*ur12*2*(p2*q)+2*(p2*u12));
      }
      chi[i]  += G12*ur12;
      chi[j]  -= G12*ur12;
      mat[i][i] += dq1;
      mat[j][j] += dq2;
      mat[i][j] = -dq2;
      mat[j][i] = -dq1;
    }
  }
  U /= U.mCalc();
  double eps=0.0;
  for(int i=0;i<NV;i++) {
    mat[NV-1][i]=2*(p[i]*U)/NV;
    eps += abs(chi[i]);
  }
  chi[NV-1]=(U*R)/NV - tStart;
  mat[NV-1][NV]= 1.0;

  if(eps > 1e-3) {
    cout << "Constraints::solveConstraints time constraints break? eps= "<< eps<<endl;
  }

  // fix time by Newton method.
  for (int k=0; k<NV; k++) {
    if (!mat[k][k]) {
      cout << "Constraints::makeConstraints singlular matirx "<< mat[k][k] << endl;
      exit(1);
    }
    for (int i=k+1; i<NV; i++) {
      double f = mat[i][k]/mat[k][k];
      for (int j=k+1; j<=NV; j++) mat[i][j] -= mat[k][j]*f;
      mat[i][k] = 0;
    }
  }
  // Start calculating from last equation up to the first.
  for (int i = NV-1; i >= 0; i--) {
    for (int j=i+1; j<NV; j++) {
      mat[i][NV] -= mat[i][j]*mat[j][NV];
    }
    mat[i][NV] /= mat[i][i];
    lambda2[i]=2*mat[i][NV];
  }

  return lambda2;
}

double Constraints::fixConstraints(vector<Pythia8::Vec4>& r,vector<Pythia8::Vec4>& p)
{
  int NV=r.size();
  if(NV ==1) return 0.0;

  pW4.assign(NV,0.0);
  double eps=10.0;
  for(int it=0;it<50;it++) {
    eps = makeConstraints(r,p);
    cout << it << " Constraints::fixConstrants eps= "<< eps<<endl;
    if(eps < 1e-5) break;
  } // loop over iteration

  if(eps > 1e-5) {
    cout << "Constraints:fixConstraints does not converge eps= "<< eps <<endl;
    //exit(1);
  }
  return eps;

}

} // end namespace jam2
