#ifndef jam2_initcond_BoostedTwoNuclei_h
#define jam2_initcond_BoostedTwoNuclei_h

#include <cmath>
#include <string>
#include <jam2/collision/Collision.h>
#include <jam2/initcond/InitialCondition.h>
#include <jam2/hadrons/JamParticleData.h>
#include <Pythia8/ParticleData.h>
#include <Pythia8/HIUserHooks.h>
#include <jam2/initcond/Constraints.h>

namespace jam2 {

class BoostedTwoNuclei: public InitialCondition
{
protected:
    Pythia8::NucleusModel* proj;
    Pythia8::NucleusModel* targ;
    vector<Pythia8::Nucleon> projNucl, targNucl;
    vector<double> mProj,mTarg;
    vector<Pythia8::Vec4> pProj, pTarg, rProj,rTarg;
    std::string compFrame;
    Vec4 pTotA,pTotB;
    double gWidth,cWidth;
    double numberOfParticle;
    double mP, mN;
    Pythia8::ParticleDataEntryPtr paA, paB, paP, paN;
    Constraints *timeCon;

    int optFermiMomentum;
    bool optBoost;
    double tStart;
    bool histWS;
    const int nhist=80;
    int   nEvent;
    double rMax, pMax, dR, dP, widR, widP, facR, facP;
    double *histr, *histp, *rhor, *rhop;
    double U0=1.0, Uz=0.0;

public:
    BoostedTwoNuclei();
    BoostedTwoNuclei(Pythia8::Settings* s, JamParticleData* pd,
	    Pythia8::Rndm* r);
    virtual ~BoostedTwoNuclei() {
	if(histWS) printHist();
	delete proj;
	delete targ;
        delete [] histr;
        delete [] histp;
	if(outputIni) iniOfs.close();
	delete timeCon;
    }

    void init();
    void generate(Collision* event,int mode=0);
    vector<Pythia8::Vec4> generateFermiMomentum(vector<Pythia8::Nucleon>& nucl,vector<double>& m,int tag);
    //void fixConstraints(vector<Pythia8::Vec4>& r,vector<Pythia8::Vec4>& p);
    //static double makeConstraints(std::vector<Pythia8::Vec4>& r,std::vector<Pythia8::Vec4>& p);
    //static std::vector<double> solveConstraints(std::vector<Pythia8::Vec4>& r,std::vector<Pythia8::Vec4>& p,double m);

    double getNumberOfParticle() const {return numberOfParticle;}
    int    getAproj() const {return proj->A();}
    int    getZproj() const {return proj->Z();}
    int    getAtarg() const {return targ->A();}
    int    getZtarg() const {return targ->Z();}
    double getRTarg() const {return targ->R();}
    double getRProj() const {return proj->R();}
    //double getRadius(int a) { return 1.19*std::pow(a,0.333333) - 1.61*std::pow(a,-0.333333);}

    void initHist();
    void fill(vector<Pythia8::Nucleon>& nucl, std::vector<Pythia8::Vec4> pf);
    void printHist();
};

}

#endif
