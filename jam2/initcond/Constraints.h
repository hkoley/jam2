#ifndef jam2_initcond_Constranits_h
#define jam2_initcond_Constranits_h

#include <vector>
#include <Pythia8/Settings.h>

namespace jam2 {

class Constraints
{
protected:
  Pythia8::Settings *settings;
  double gWidth,cWidth;
  double tStart;
  int optGij;
  int optCollisionOrdering;
  std::vector<double> chi;
  std::vector<double> chi1,chi2;
  std::vector<double> pW4;
  std::vector<std::vector<double> > mat;

public:
  Constraints(Pythia8::Settings* s);
  ~Constraints() { }
  double lambda(const Pythia8::Vec4& p, const Pythia8::Vec4& phat,double m, double lambda);
  double makeConstraintsMatrix(std::vector<Pythia8::Vec4>& r,std::vector<Pythia8::Vec4>& p);
  void newtonRaphson(std::vector<Pythia8::Vec4>& r);
  void w4UL(std::vector<Pythia8::Vec4>& r);
  void w4LU(std::vector<Pythia8::Vec4>& r);
  double fixConstraints(std::vector<Pythia8::Vec4>& r,std::vector<Pythia8::Vec4>& p);
  double makeConstraints(std::vector<Pythia8::Vec4>& r,std::vector<Pythia8::Vec4>& p);
  std::vector<double> solveConstraints(std::vector<Pythia8::Vec4>& r,std::vector<Pythia8::Vec4>& p,double m);
};

}
#endif
