#include <jam2/initcond/BoostedTwoNuclei.h>
#include <jam2/initcond/Nucleus.h>
#include <jam2/collision/Collision.h>
#include <cstdlib>

using namespace std;
using namespace Pythia8;

namespace jam2 {

BoostedTwoNuclei::BoostedTwoNuclei(Settings* s, JamParticleData* p, Rndm* r)
    : InitialCondition(s,p,r)
{
  eCM=settings->parm("Beams:eCM");
  pLab=settings->parm("Beams:pLab");
  eLab=settings->parm("Beams:eLab");
  bMin=settings->parm("Beams:bmin");
  bMax=settings->parm("Beams:bmax");
  zSep=settings->parm("Beams:zseparation");
  compFrame=settings->word("Beams:compFrame");
  optBoost=settings->flag("Beams:optBoost");
  optFermiMomentum=settings->mode("Cascade:optFermiMomentum");
  gWidth=settings->parm("MeanField:gaussWidth");
  cWidth=settings->parm("Cascade:gaussWidth");

  paP = particleData->particleDataEntryPtr(2212);
  paN = particleData->particleDataEntryPtr(2112);
  mP=paP->m0();
  mN=paN->m0();

  histWS = settings->flag("HeavyIon:histWS");
  histr = 0; histp=0;
  if(histWS) initHist();

  //outputIni= settings->flag("HeavyIon:optOutputInitialPhaseSpace");
  //if(outputIni) iniOfs.open(settings->word("HeavyIon:fileNameOutputInitialPhaseSpace"));
  tStart = settings->parm("Cascade:timeStart");

  timeCon = new Constraints(settings);
}

void BoostedTwoNuclei::init()
{
    string nuclA = settings->word("Beams:beamA");
    string nuclB = settings->word("Beams:beamB");
    pair<int,int> a = findAZ(nuclA);
    pair<int,int> b = findAZ(nuclB);
    int idProj=a.first;
    int idTarg=b.first;
    izA=a.second;
    izB=b.second;

    //proj = new GLISSANDOModel();
    proj = new Nucleus();
    proj->initPtr(idProj, *settings,*particleData,*rndm);
    proj->init();
    dynamic_cast<Nucleus*>(proj)->setParam();
    //targ = new GLISSANDOModel();
    targ = new Nucleus();
    targ->initPtr(idTarg, *settings,*particleData,*rndm);
    targ->init();
    dynamic_cast<Nucleus*>(targ)->setParam();

    masA = atoi(nuclA.c_str());
    masB = atoi(nuclB.c_str());
    if(masA==0) masA=1;
    if(masB==0) masB=1;
    
    if(masA>1) mA=0.5*(mP+mN);
    else mA= particleData->particleDataEntryPtr(idProj)->m0();
    if(masB>1) mB=0.5*(mP+mN);
    else mB= particleData->particleDataEntryPtr(idTarg)->m0();

    // compFrame=nn: equal speed frame
    pzAnn  = 0.5 * sqrtpos( (eCM + mA + mB) * (eCM - mA - mB)
           * (eCM - mA + mB) * (eCM + mA - mB) ) / eCM;
    pzBnn  = -pzAnn;

    // compFrame=="cm"
    double m1 = mA*masA;
    double m2 = mB*masB;
    //double ecm = sqrt(pow2(eLab*nA + m1 + m2) - pow2(pLab*nA));
    double e1=sqrt(mA*mA+pzAnn*pzAnn)*masA;
    double e2=sqrt(mB*mB+pzBnn*pzBnn)*masB;
    double srt=sqrt(pow2(e1+e2)-pow2(pzAnn*masA+pzBnn*masB));
    double  pz = 0.5 * sqrtpos( (srt + m1 + m2) * (srt - m1 - m2)
           * (srt - m1 + m2) * (srt + m1 - m2) ) / srt;
    pzAcm =   pz / masA;
    pzBcm = - pz / masB;

    // U0 and Uz in this part will not be used now.
    pHat=Vec4(0.0,0.0,0.0,1.0);
    //if(optCollisionOrdering>100) {
    if(optBoost) {
      // equal time at the global CM frame.
      if(optTimeFixation==1) {
        pBoostA = pzAcm;
        pBoostB = pzBcm;
        if(compFrame=="nn") {
	  double enn=sqrt(mB*mB+pzBnn*pzBnn);
	  double ecm=sqrt(mB*mB+pzBcm*pzBcm);
          U0=(enn*ecm-pzBnn*pzBcm)/(mB*mB); Uz=(enn*pzBcm-ecm*pzBnn)/(mB*mB);
	} else if(compFrame=="lab") {
          U0=sqrt(mB*mB+pzBcm*pzBcm)/mB; Uz=pzBcm/mB;
	}
        double e=pHat[0];
        pHat[0]=U0*e       - Uz*pHat[3];
        pHat[3]=U0*pHat[3] - Uz*e;

      // equal time at the equal speed frame.
      } else if(optTimeFixation==2) {
        pBoostA = pzAnn;
        pBoostB = pzBnn;
        if(compFrame=="cm") {
	  double enn=sqrt(mB*mB+pzBnn*pzBnn);
	  double ecm=sqrt(mB*mB+pzBcm*pzBcm);
          U0=(enn*ecm-pzBnn*pzBcm)/(mB*mB); Uz=(ecm*pzBnn-enn*pzBcm)/(mB*mB);
	} else if(compFrame=="lab") {
          U0=sqrt(mB*mB+pzBnn*pzBnn)/mB; Uz=pzBnn/mB;
	}
        double e=pHat[0];
        pHat[0]=U0*e       - Uz*pHat[3];
        pHat[3]=U0*pHat[3] - Uz*e;

      // equal time at the lab. frame.
      } else {
        pBoostA = pLab;
        pBoostB = 0.0;
        if(compFrame=="nn") {
          U0=sqrt(mB*mB+pzBnn*pzBnn)/mB; Uz=-pzBnn/mB;
	} else if(compFrame=="cm") {
	}
      }

    } else {
      if(compFrame=="nn") {
        pBoostA = pzAnn;
        pBoostB = pzBnn;
      } else if(compFrame=="cm") {
        pBoostA = pzAcm;
        pBoostB = pzBcm;
      } else {
        pBoostA = pLab;
        pBoostB = 0.0;
      }
    }

    eA = sqrt(mA*mA + pBoostA*pBoostA);
    eB = sqrt(mB*mB + pBoostB*pBoostB);
    gamA= eA/mA;
    gamB= eB/mB;
    yCM = 0.5*log((eA + pBoostA)/(eA - pBoostA));

}

void BoostedTwoNuclei::generate(Collision* event,int mode)
{
  // Generate impact parameter.
  impactPar = setImpactPar();

  pTotA=0.0;pTotB=0.0;
  // Loop over test particles.
  for(int iev=0;iev<overSample;iev++) {

    // Generate the position of nucleons inside nucleus.
    projNucl = proj->generate();
    targNucl = targ->generate();
    mProj.resize(projNucl.size());
    mTarg.resize(targNucl.size());
    rProj.resize(projNucl.size());
    rTarg.resize(targNucl.size());

    vector<double> lambdaA(projNucl.size()), lambdaB(targNucl.size());

    // Generate Fermi momentum.
    pProj=generateFermiMomentum(projNucl,mProj,1);
    pTarg=generateFermiMomentum(targNucl,mTarg,2);
    if(histWS) {
      fill(projNucl,pProj);
      fill(targNucl,pTarg);
    }

    // Set parameters for coordinates.
    double zmaxA=-100;
    for ( int i = 0, N = projNucl.size(); i < N; ++i )
	zmaxA=max(zmaxA,projNucl[i].nPos().pz());
    double rzA = (proj->R()+1.0);
    if(zmaxA> rzA + gamA*zSep) {
      rzA=zmaxA;
    }

    double zminB=100;
    for ( int i = 0, N = targNucl.size(); i < N; ++i )
	zminB=min(zminB,targNucl[i].nPos().pz()/gamB);
    double rzB = (targ->R()+1.0);
    if(zminB < -rzB - gamB*zSep) {
      rzB=zminB;
    }

    double xA,zA,xB,zB;
    //if(compFrame != "lab" || (optCollisionOrdering >= 103 && optCollisionOrdering <=110)){
    //if(compFrame != "lab" || optCollisionOrdering > 100){
    if(compFrame != "lab"){
       xA= impactPar/2.0;
       zA=  -abs(rzA)/gamA - zSep;
       xB = -impactPar/2.0;
       zB =  abs(rzB)/gamB + zSep;
    } else {
       zA=  -abs(rzA)/gamA - zSep -abs(rzB);
       zB = 0.0;
       xA= impactPar;
       xB = 0.0;
       //xA =  impactPar/2.0;
       //xB = -impactPar/2.0;
    }

    Vec4 bvecA( xA, 0.0, zA, 0.0);
    Vec4 bvecB( xB, 0.0, zB, 0.0);

    Vec4 ptotA=0.0,ptotB=0.0;
    // Sorge's time constrants.
    if(optCollisionOrdering==121) {

      for ( int i = 0, N = projNucl.size(); i < N; ++i ) {
        rProj[i] = projNucl[i].nPos();
        rProj[i] += bvecA;
        rProj[i][0]= tStart;
      }
      timeCon->fixConstraints(rProj,pProj);
      for ( int i = 0, N = projNucl.size(); i < N; ++i ) {
        double t=rProj[i][0];
        rProj[i][0]=gamA*(t + pBoostA/eA*rProj[i][3]);
        rProj[i][3]=gamA*(rProj[i][3] + pBoostA/eA*t);
        double ep=pProj[i][0];
        pProj[i][0]=gamA*(ep + pBoostA/eA*pProj[i][3]);
        pProj[i][3]=gamA*(pProj[i][3] + pBoostA/eA*ep);
        ptotA += pProj[i];
        //pProj[i][0]=sqrt(pow2(mProj[i])+pProj[i].pAbs2());
      }
      lambdaA = timeCon->solveConstraints(rProj,pProj,mA);
      //cout << "after boost proj eps= "<< makeConstraints(rProj,pProj) <<endl;
      //for(int i=0;i < (int)projNucl.size();i++) {
      //  cout << "proj lamba= "<< lambdaA[i] << " t= "<< rProj[i][0] << " z= "<< rProj[i][3] <<endl;
      //}

      for ( int i = 0, N = targNucl.size(); i < N; ++i ) {
        rTarg[i] = targNucl[i].nPos();
        rTarg[i] += bvecB;
        rTarg[i][0]= tStart;
      }
      timeCon->fixConstraints(rTarg,pTarg);
      for ( int i = 0, N = targNucl.size(); i < N; ++i ) {
        double t=rTarg[i][0];
        rTarg[i][0]=gamB*(t + pBoostB/eB*rTarg[i][3]);
        rTarg[i][3]=gamB*(rTarg[i][3] + pBoostB/eB*t);
        double ep=pTarg[i][0];
        pTarg[i][0]=gamB*(ep + pBoostB/eB*pTarg[i][3]);
        pTarg[i][3]=gamB*(pTarg[i][3] + pBoostB/eB*ep);
        ptotB += pTarg[i];
      }
      lambdaB = timeCon->solveConstraints(rTarg,pTarg,mB);
      //cout << "after boost targ eps= "<< makeConstraints(rTarg,pTarg) <<endl;
      //for(int i=0;i < (int)targNucl.size();i++) {
      //  cout << "targ lamba= "<< lambdaB[i] << " t= "<< rTarg[i][0] << " z= "<< rTarg[i][3] <<endl;
      //}

    } else {

    // boost and shift projectile.
    for ( int i = 0, N = projNucl.size(); i < N; ++i ) {
      projNucl[i].bShift(bvecA);
      rProj[i] = projNucl[i].nPos();
      rProj[i][3] /= gamA; rProj[i] += bvecA; rProj[i][0]= tStart;

      pProj[i][3] = gamA*pProj[i][3] + pBoostA;
      pProj[i][0]=sqrt(pow2(mProj[i]) + pProj[i].pAbs2());
      if(optCollisionOrdering>=103 && optCollisionOrdering <110) 
	rProj[i][0]=(tStart*mProj[i]+dot3(pProj[i],rProj[i]))/pProj[i][0];
      lambdaA[i] = 1.0/(pProj[i]*pHat);
      ptotA += pProj[i];

    }
    if(optCollisionOrdering==120)  timeCon->fixConstraints(rProj,pProj);

    /*
    int it=0, ip=0;
  for(int i=0;i<rProj.size();i++) {
    if(rProj[i].m2Calc()>0.0) ip++;
  for(int j=i+1;j<rProj.size();j++) {
    double dr2=(rProj[i]-rProj[j]).m2Calc();
    if(dr2>0.0) {
      cout << "i= "<< i << " j= "<< j << " dr2= "<< dr2<<endl;
      it++;
    }
  }
  }
  if(it>0) cout << " it= "<< it << " ip= "<< ip<<endl;
  */

    // boost and shift target.
    for ( int i = 0, N = targNucl.size(); i < N; ++i ) {
      targNucl[i].bShift(bvecB);
      rTarg[i] = targNucl[i].nPos();
      rTarg[i][3] /= gamB; rTarg[i] += bvecB; rTarg[i][0]= tStart;

      pTarg[i][3] = gamB*pTarg[i][3] + pBoostB;
      pTarg[i][0] = sqrt(pow2(mTarg[i]) + pTarg[i].pAbs2());
      if(optCollisionOrdering>=103 && optCollisionOrdering <110) 
	rTarg[i][0]=(tStart*mTarg[i]+dot3(pTarg[i],rTarg[i]))/pTarg[i][0];
      lambdaB[i] = 1.0/(pTarg[i]*pHat);
      ptotB += pTarg[i];
    }
    if(optCollisionOrdering==120)  timeCon->fixConstraints(rTarg,pTarg);

    }

    //if(optCollisionOrdering > 100) {
    if(optBoost) {
      pHat=Vec4(0.0,0.0,0.0,1.0);
      if(compFrame=="lab") {
	double m=ptotB.mCalc();
	U0 = ptotB[0]/m;
	Uz = ptotB[3]/m;
      } else if(compFrame=="cm") {
	Vec4 ptot=ptotA+ptotB;
	double m=ptot.mCalc();
	U0 = ptot[0]/m;
	Uz = ptot[3]/m;
      } else if(compFrame=="nn" && optTimeFixation !=2) {
	double m=ptotB.mCalc();
	double u0b = ptotB[0]/m;
	double uzb = ptotB[3]/m;
	double u0c = sqrt(mB*mB+pzBnn*pzBnn)/mB;
	double uzc = pzBnn/mB;
	U0 = u0c*u0b-uzc*uzb;
	Uz = u0c*uzb-uzc*u0b;
      }

      double e=pHat[0];
      pHat[0]=U0*e       - Uz*pHat[3];
      pHat[3]=U0*pHat[3] - Uz*e;
      for ( int i = 0, N = projNucl.size(); i < N; ++i ) {
          double t=rProj[i][0];
          rProj[i][0]=U0*t - Uz*rProj[i][3];
          rProj[i][3]=U0*rProj[i][3] -Uz*t;
          double ep=pProj[i][0];
          pProj[i][0]=U0*ep - Uz*pProj[i][3];
          pProj[i][3]=U0*pProj[i][3] -Uz*ep;
          lambdaA[i]=timeCon->lambda(pProj[i],pHat,mProj[i],lambdaA[i]);
      }

      for ( int i = 0, N = targNucl.size(); i < N; ++i ) {
          double t=rTarg[i][0];
          rTarg[i][0]=U0*t           - Uz*rTarg[i][3];
          rTarg[i][3]=U0*rTarg[i][3] - Uz*t;
          double et=pTarg[i][0];
          pTarg[i][0]=U0*et          - Uz*pTarg[i][3];
          pTarg[i][3]=U0*pTarg[i][3] - Uz*et;
          lambdaB[i]=timeCon->lambda(pTarg[i],pHat,mTarg[i],lambdaB[i]);
      }
    }

    // put projectile nucleus into event array.
    for ( int i = 0, N = projNucl.size(); i < N; ++i ) {
      int id=projNucl[i].id();
      //Pythia8::ParticleDataEntryPtr pa = particleData->particleDataEntryPtr(projNucl[i].id());
      Pythia8::ParticleDataEntryPtr pa = id==2212 ? paP : id==2112? paN : paA;
      EventParticle* cp = new EventParticle(id,mProj[i],rProj[i],pProj[i],pa,1);
      cp->setPID(jamParticleData->pid(abs(id)));
      cp->setOnShell();
      cp->setNumberOfColl(1);
      cp->setConstQuark();
      cp->lambda(lambdaA[i]);
      if(optCollisionOrdering > 100) cp->tevol(tStart);
      if(mode==0) event->setPList(cp);
      else event->setPnewAList(cp);
      pTotA +=cp->getP();
    }

    // put target nucleus into event array.
    for ( int i = 0, N = targNucl.size(); i < N; ++i ) {
      int id=targNucl[i].id();
      //Pythia8::ParticleDataEntryPtr pa= particleData->particleDataEntryPtr(targNucl[i].id());
      Pythia8::ParticleDataEntryPtr pa = id==2212 ? paP : id==2112? paN : paB;
      EventParticle* cp = new EventParticle(id,mTarg[i],rTarg[i],pTarg[i],pa,-1);
      cp->setPID(jamParticleData->pid(id));
      cp->setOnShell();
      cp->setNumberOfColl(-1);
      cp->setConstQuark();
      cp->lambda(lambdaB[i]);
      if(optCollisionOrdering > 100) cp->tevol(tStart);
      if(mode==0) event->setPList(cp);
      else event->setPnewBList(cp);
      pTotB +=cp->getP();
    }

  //cout << " ptotA= "<< pTotA/masA << " ptotB= "<< pTotB/masB<<endl;
  //cout << " ptot= "<< pTotA+pTotB<<endl;
  //cin.get();

  } // end loop over oversampling.

  //pHat=Vec4(0.,0.,pzBnn,sqrt(mB*mB+pzBnn*pzBnn));

  pHat = (pTotA + pTotB)/overSample;
  pHat /= pHat.mCalc();

  //cout << " pHat= "<< pHat <<endl;
  //cin.get();

    if(mode !=0 ) {
      event->phat(pHat);
      event->makeCollisionListTwoNuclei(tStart,1000000.0);
      //nCollG = event->getInterListSize(); // collision number.
      nCollG = event->getNumberOfGlauberCollision(); // collision number.
      nPartG = event->getNumberOfParticipants();

      // output initial phase space
      if(outputIni) {
	iniOfs << event->plist.size()<<endl;
	for(auto& p: event->plist) {
          Vec4 r = p->getR();
          iniOfs << setw(3) << p->getStatus()
              << setw(12) << p->getID()
              << setw(15) << fixed << p->getMass()
              << scientific
              << setw(16) << setprecision(8) << r[0]
              << setw(16) << setprecision(8) << r[1]
              << setw(16) << setprecision(8) << r[2]
              << setw(16) << setprecision(8) << r[3]
              << setw(16) << setprecision(8) << p->getPe()
	      << setw(16) << setprecision(8) << p->getPx()
              << setw(16) << setprecision(8) << p->getPy()
              << setw(16) << setprecision(8) << p->getPz()
	      <<endl;
	}
      }
    }

    //event->makeCollisionList();

}

vector<Vec4> BoostedTwoNuclei::generateFermiMomentum(vector<Pythia8::Nucleon>& nucl,vector<double>& mass,int tag)
{
  int N=nucl.size();
  vector<Vec4> pv(N,0.0);

  // assign mass
  mass[0]= tag == 1 ? mA : mB;

  //if(N>1) for(int i=0;i<N;i++) mass[i]=0.5*(mP+mN);

  if(N ==1 || optFermiMomentum==0) return pv;

  // Compute local density.
  double fac=pow(4*M_PI*gWidth,1.5);
  std::vector<double> rhon(N,0.0);
  for(int i=0;i<N; i++) {
    for(int j=i+1; j<N; j++) {
      Vec4 dr=nucl[i].nPos() - nucl[j].nPos();
      //double den=(dr.px()*dr.px() + dr.py()*dr.py()+dr.pz()*dr.pz());
      double den = exp(-dr.pAbs2()/2/gWidth)/fac;
      rhon[i] += den;
      rhon[j] += den;
    }
  }

  // Generate Fermi momentum using the local density.
    Vec4 ptot=0.0;
    for(int i=0;i<N; i++) {
	double pfermi=HBARC*pow(1.5*M_PI*M_PI*rhon[i],1.0/3.0);
	double pf=pfermi*pow(rndm->flat(),1.0/3.0);
	double cth=1.0 - 2.0*rndm->flat();
	double sth=sqrt(1.0-cth*cth);
	double phi=2*M_PI*rndm->flat();
	pv[i][3]=pf*cth;
	pv[i][1]=pf*sth*cos(phi);
	pv[i][2]=pf*sth*sin(phi);
	ptot += pv[i];
    }

    ptot /= N;
    //Vec4 ptotal = 0.0;
    for(int i=0;i<N; i++) {
	int id=nucl[i].id();
	mass[i]=particleData->m0(id);
        //mass[i]=0.5*(mP+mN);
	pv[i] -= ptot;
	pv[i][0]=sqrt(mass[i]*mass[i] + pv[i].pAbs2());
	//ptotal += pv[i];
    }

    // check total momentum if it is zero.
    //cout << N << " generateFermi p= "<< ptotal << endl;
    
    return pv;
}

void BoostedTwoNuclei::printHist()
{
  ofstream ofst("ws.hist");
  ofst << "# width= "<< widR/2.0 << endl;
  for( int i=0; i< nhist; i++) {
    ofst << setw(7) << i*dR 
 	<< setw(12) << histr[i]/nEvent/overSample
	<< setw(12) << rhor[i]/nEvent/overSample
        << setw(7) << i*dP
	<< setw(12) << histp[i]/nEvent/overSample
	<< setw(12) << rhop[i]/nEvent/overSample
	<<endl;
  }

  ofst.close();
}

void BoostedTwoNuclei::fill(vector<Pythia8::Nucleon>& nucl, vector<Vec4> pf)
{
  nEvent++;
  int N = nucl.size();
  for ( int i = 0;  i < N; ++i ) {
    Vec4 r = nucl[i].nPos();
    Vec4 p = pf[i]/HBARC;
    double rr = r.pAbs2();
    double pp = p.pAbs2();
    int ir = int(sqrt(rr)/dR+0.5);
    int ip = min(nhist,int(sqrt(pp)/dP+0.5));
    histr[ir] += 1.0/(4*M_PI*rr*dR);
    histp[ip] += 1.0/(4*M_PI*pp*dP);
    for (int l=0;l<nhist;l++) {
      Vec4 x(l*dR, 0.0, 0.0, 0.0);
      Vec4 px(l*dP/HBARC,0.0, 0.0, 0.0);
      rhor[l] += exp(-(r-x).pAbs2()/widR)*facR;
      rhop[l] += exp(-(p-px).pAbs2()/widP)*facP;
    }
  }
}

void BoostedTwoNuclei::initHist()
{
  nEvent = 0;
  rMax=15.0; pMax=2.0;
  dR=rMax / nhist;
  dP=pMax / nhist;
  histr = new double [nhist];
  histp = new double [nhist];
  rhor = new double [nhist];
  rhop = new double [nhist];
  for(int i=0;i<nhist;i++) {
    histr[i]=histp[i]=0.0;
    rhor[i]=rhop[i]=0.0;
  }
  widR = 2 * settings->parm("MeanField:gaussWidth");
  widP = 1.0/widR;
  facR=1.0/pow(M_PI*widR,1.5);
  facP=1.0/pow(M_PI*widP,1.5);
}

}
