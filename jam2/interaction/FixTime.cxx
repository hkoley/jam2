#include <jam2/interaction/FixTime.h>
#include <Pythia8/PythiaStdlib.h>
#include <jam2/hadrons/JamStdlib.h>

namespace jam2 {

using namespace std;

FixTime::FixTime(Pythia8::Info* inf,Pythia8::Settings* s)
     : info(inf), settings(s)
{
  // Propagation by canonical momentum in RQMD mode
  optVectorPotential=settings->mode("MeanField:optVectorPotential");
  optVdot=settings->mode("MeanField:optVdot");
  optPropagate=0;
  if(optVectorPotential==1 && optVdot==0) optPropagate=1;

  optCollisionOrdering = settings->mode("Cascade:optCollisionOrder");
  lambdaLag = settings->parm("Cascade:LagrangeMultiplier");

}

FixTime::~FixTime() 
{
}

// tc:    time of particle at the collision.
// cTime: evolution parameter at the collision
void FixTime::setEvolutionTime(EventParticle* pa,double tc,double cTime,double lambda0,int proc)
{
  Vec4 r = pa->getR();
  double taup=r[0];
  Vec4 pnew = optPropagate == 0 ? pa->getP() : pa->getPkin();
  double lambda=1.0/pnew[0];
  if(optCollisionOrdering<100) {
    pa->tevol(taup);
    pa->lambda(lambda);
    return;
  }

  if(optCollisionOrdering==101) {
      lambda = 1.0/(pnew*pHat);
      //double vel0=pnew.e()/(pnew*pHat);
      //taup = (r[0] - tc)/vel0 + cTime;
      taup=pHat*r;
  } else if(optCollisionOrdering>=120) {
      if(isinf(lambda0) || isnan(lambda0)){
	cout << " id= "<< pa->getID() << " lam= "<<lambda0 << " inf= "<< isinf(lambda0) << " proc= "<< proc <<endl;
	exit(1);
      }
      lambda = lambda0;
      taup = (r[0] - tc)/(pnew[0]*lambda) + cTime;
  } else if(optCollisionOrdering<110) {
      double m = pa->getMass();
      if(m ==0.0) m=1.0;
      double vel0 = pnew.e()/m;
      taup = (r[0] - tc)/vel0 + cTime;
      lambda=1.0/m;
      if(optCollisionOrdering==103) { // change tau of particle
	taup = r*pnew/m;
      }else if(optCollisionOrdering==104) { // change time of particle
	double tnew=(m*taup + dot3(pnew,r)) / pnew.e();
	pa->setT(tnew);
      }else if(optCollisionOrdering==105) { // change energy of particle
	double enew = (m*taup + dot3(pnew,r)) / r[0];
	double msq=enew*enew - pnew.pAbs2();
	if(msq<0) {
	  cout << " msq= "<< msq<<endl;
	} else {
	  //pa->setE(enew);
	  pa->setPotS(sqrt(msq)-m);
	  cout << " spot? "<< sqrt(msq)-m<<endl;
	  cin.get();
	}
      }
  } else {
      lambda = lambdaLag;
      taup = (r[0] - tc)/(pnew[0]*lambda) + cTime;
  }
  pa->tevol(taup);
  pa->lambda(lambda);

}

} // end namespace jam2.
