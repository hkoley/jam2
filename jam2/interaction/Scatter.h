#ifndef jam2_interaction_Scatter_h
#define jam2_interaction_Scatter_h

#include <Pythia8/Basics.h>
#include <Pythia8/ParticleData.h>
#include <jam2/collision/EventParticle.h>
#include <jam2/hadrons/JamParticleData.h>
#include <jam2/collision/Collision.h>
#include <jam2/collision/TwoBodyInterList.h>
#include <jam2/xsection/CollisionPair.h>
#include <jam2/xsection/CrossSection.h>
#include <jam2/interaction/SoftStrings.h>
#include <jam2/interaction/JPythia.h>
#include <jam2/interaction/FixTime.h>

#include <vector>
#include <cmath>

namespace jam2 {

using Pythia8::Vec4;

class Scatter
{
protected:
    Pythia8::Info*  info;
    Pythia8::Settings* settings;
    JPythia  *pythia=0, *pythia2=0;
    SoftStrings* scatt;
    Pythia8::Vec4 pHat;
    FixTime* fixtime;
    Pythia8::Rndm* rndm;
    int channel;
    int softModel;
    int printColl;
    bool inelOnly;
    double eCMPythia,eMinPert,eMinPert2,eWidthPert,eMinPertMB,eMinPertMB2;

public:
  enum Channel{ELASTIC, RESONANCE, SOFT, HARD, DECAY, ABSORPTION,DIFFRACTIVE, BBANIHILATION};

  Scatter(Pythia8::Info* inf,Pythia8::Settings* s,JamParticleData* jd,
	    CrossSection* xs, Pythia8::Pythia* py, 
            Pythia8::StringFlav* flav,
	    FixTime* ft,
	    Pythia8::Rndm* r);
  virtual ~Scatter();

  virtual void scatter(InterList* inter,std::vector<EventParticle*>& outgoing,Collision* evvent);

  void setPythia(JPythia* py) {pythia=py;}
  void setPythia2(JPythia* py) {pythia2=py;}
  int usePythia(double ecm,CollisionPair cpair);
  int getChannel() const {return scatt->getChannel();}
  void phat(Vec4 p) {pHat=p;scatt->phat(p);}

};
}
#endif

