#include <jam2/xsection/CG.h>
#include <iostream>
#include <cmath>

namespace jam2 {
using namespace std;

double ClebschGordan::bico(double a,double b)
{
  double x=a-b;
  if(x < 0.0) return 0.0;
  if(x == 0.0) return 1.0;
  if(b < 0.0) return 0.0;
  if(b == 0.0) return 1.0;

  double y=min(b,x);
  double bico=a/y;
  int j=y-1.0;
  if(j < 1) return bico;

  for(int i=1;i<=j;i++) {
    double u=double(i);
    bico *= (a-u)/(y-u);
  }

  return bico;
}

// Clebsch-Gordan coefficient
// cofcg(j1,j2,j,m1,m2,m) = <j1,j2;m1,m2|j,m>
double ClebschGordan::cofcg(double a,double b,double c,double x,double y,double z)
{
  if(abs(x) > a) return 0.0;
  if(abs(y) > b) return 0.0;
  if(abs(z) > c) return 0.0;

  if(c-a-b > 0.0 ) return 0.0;
  if(a-b-c > 0.0 ) return 0.0;
  if(b-a-c > 0.0 ) return 0.0;
  if(z-x-y != 0.0) return 0.0;

  int nmax= min(c-a+b,c+z);
  int nmin= max(0.0,b-a+z);
  double sum=0.0;
  double x1=a+x;
  double y1=b+y;
  double z1=c+z;
  double x2=a-x;
  double y2=b-y;
  double z2=c-z;
  double z3=2.0*c+1.0;
  double r=a+b+c+1.0;
  double s=c+b+x;
  double t=b+c-a;
  int j=y1;
  for(int i=nmin;i<=nmax;i++) {
    double u=double(i);
    sum += pow(-1.0,i)*bico(z1,u)*bico(s-u,x1)*bico(x2+u,y1);
  }
  sum *=pow(-1.0,j);

  double g=1.0;
  int l=1;
  if(x < 0.0) {
    g=1.0/bico(x2,x1);
    l=-1;
  } else if(x == 0.0) {
    g=1.0;
    l=1;
  } else {
    g=bico(x1,x2);
    l=1;
  }

  if(y < 0.0) {
    g /= bico(y2,y1);
    l -= 2;
  } else if(y == 0.0) {
    g *= 1.0;
    l += 2;
  } else {
    g *= bico(y1,y2);
    l += 2;
  }

  if(z < 0.0) {
    g *= bico(z2,z1);
    l -= 3;
  } else if(z == 0.0) {
    g *= 1.0;
    ++l;
  } else {
    g /= bico(z1,z2);
    ++l;
  }

  l=(8+l)/2;

  if(l == 1)
    g *= bico(-2.0*z,-2.0*x);
  else if(l == 2)
    g /= bico(-2.0*y,-2.00*z);
  else if(l == 3)
    g /= bico(-2.0*x,-2.0*z);
  else if(l == 4)
    g *= bico(2.0*x,2.0*z);
  else if(l == 5)
    g *= bico(2.0*y,2.0*z);
  else if(l == 6)
    g /= bico(2.0*z,2.0*x);
  else {
    cout <<  "error " << l <<endl;
    exit(1);
  }

  return sum*sqrt(g*z3/(bico(r-1.0,2.0*c)*bico(2.0*c,t)*r));
}

} // end namespace jam2
