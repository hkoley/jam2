#ifndef jam2_xsection_CG_h
#define jam2_xsection_CG_h

namespace jam2 {

// Clebsch-Gordan Coefficient
class ClebschGordan 
{
  public:
  static double cofcg(double a,double b,double c,double x,double y,double z);
  static double bico (double a,double b);

  // cofcgi(j1,j2,j,m1,m2,m) = <j1,j2;m1,m2|j,m>
  static double cofcgi(int j1,int j2,int j,int jz1,int jz2,int jm) {
    return cofcg(double(j1)/2,double(j2)/2,double(j)/2,double(jz1)/2,double(jz2)/2,double(jm)/2);
  }
};

}
#endif

