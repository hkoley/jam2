#ifndef jam2_meanfield_RQMDv_h
#define jam2_meanfield_RQMDv_h

#include <jam2/meanfield/MeanField.h>
#include <jam2/meanfield/VectorPotential2.h>

namespace jam2 {

using Pythia8::Vec4;

class RQMDv : public MeanField
{
protected:
  std::vector<EventParticle*> part; // particles for potential interaction
  std::vector<std::vector<double> > rhom; // rho_{ij}
  std::vector<double> rho;      // invariant baryon density
  std::vector<double> lambda;   // Lagrange multipliers
  std::vector<std::vector<double> > rhog;   // rho^{gam-1}
  std::vector<Vec4> vmom4; // momentum-dependent vector potential
  std::vector<Vec4> JB;       // baryon current
  std::vector<Vec4> Vdot;    // time derivative of vector potential
  std::vector<Vec4> force;  // force for p
  std::vector<Vec4> forcer; // force for r
  static bool firstCall;
  int transportModel;
  int overSample;
  int nV; // numbef of terms in the vector potential.
  VectorPotential2 *potential;
  std::vector<double> tFac, bFac, pFac;
  //double vex1,vex2,pmu1,pmu2;
  double wG,facG;
  Pythia8::Vec4 devV1,devV2;
  double cutOffPot;
  bool isPotTable=false;
  double rhoMin;

  // for non-linear sigma
  //double mSigma, mOmega, mSigmaFM2,mSigma2;
  //double gs,gv,g2,g3,g4,gsp,gvp,GS,G2,G3,t1f;
  //double G4,G24,G34,GS4;
  //double scalarDens;


public:
  RQMDv(Pythia8::Settings* s);
  ~RQMDv();
  void evolution(std::list<EventParticle*>& plist,double t, double dt,int step);
  void init(std::list<EventParticle*>& plist) { };
  Pythia8::Vec4 computeEnergy(std::list<EventParticle*>& plist,int step);
  void singleParticlePotential();
  //void setScalarPotential();
  void setFreeLagrangeMultiplier();
  void findLagrangeMultiplier(std::vector<std::vector<double> >& mat,int n);

  void devV(const Vec4& Ai, const Vec4& Aj, const Vec4& v1, const Vec4& v2, const double p01, const double p02);
  void Vmd(double p1, double p2,double vfac1,double vfac2,double fi1,double fi2,double fj1,double fj2);
  //void dVdns(double rij, double rji);
  //void dVdmd(double p1, double p2,double fs,double fv, double pmi1,double pmi2,double pmj1,double pmj2);

  std::vector<EventParticle*>& getParticle() {return part;}
  int particleSize()                         {return part.size();}
  void add(EventParticle* p) { 
      potential->setPotentialParam(p);
      part.push_back(p); }

  //void addRhos(int i, double a)                {rhos[i] +=a;}
  void addRho(int i, double a)                 {rho[i] +=a;}
  //void addVmoms(int i, double a)               {vmoms[i] +=a;}
  void addVmom4(int i, const Pythia8::Vec4& a) {vmom4[i] +=a;}
  void addJB(int i, const Pythia8::Vec4& a)    {JB[i] +=a;}
  void addForce(int i, const Pythia8::Vec4& f) {force[i] +=f;}
  void addForceR(int i, const Pythia8::Vec4& f){forcer[i] +=f;}
  void addVdot(int i, const Pythia8::Vec4& v)  {Vdot[i] +=v;}
  double getRhog(int i, int j)                 {return rhog[i][j];}
  double getRho(int i)                         {return rho[i];}
  //double getRhos(int i)                        {return rhos[i];}
  //double getRhosg(int i)                       {return rhosg[i];}
  //double getVmoms(int i)                       {return vmoms[i];}
  Pythia8::Vec4   getJB(int i)                 {return JB[i];}
  Pythia8::Vec4   getForce(int i)              {return force[i];}
  Pythia8::Vec4   getForceR(int i)             {return forcer[i];}

  void qmdMatrix();
  void qmdMatrix2();
  void computeForce();
  void computeForceMatrix();
  void computeForceMatrix2();
  void computeBUUForce();

  void computeVdot();
  Vec4 getPcan(Vec4 p, Vec4 v) {
    double msq=p.m2Calc();
    p += v; p[0] = sqrt(msq + p.pAbs2());
    return p;
  }
  Vec4 getPkin(Vec4 p, Vec4 v, double m) {
    p -= v; p[0] = sqrt(m*m + p.pAbs2());
    return p;
  }

  Pythia8::Vec4 facV(int i, Vec4& p);
  double devVme(double psq,double pmu,double vex) {
    double fac1 = 1.0 - psq/pmu;
    return -vex/(pmu*fac1*fac1);
  }
  double devVmd(double psq,double pmu,double vex) {
    return vex/(1.0 - psq/pmu);
  }

};
}
#endif
