#ifndef jam2_meanfield_MeanField_h
#define jam2_meanfield_MeanField_h

#include <Pythia8/Basics.h>
#include <Pythia8/Settings.h>
#include <jam2/collision/EventParticle.h>
#include <jam2/meanfield/TwoBodyDistance.h>

namespace jam2 {

class MeanField
{
protected:
  Pythia8::Settings* settings;

  int NV, selfInt;
  bool withMomDep;
  int eosType,optPotential, isDebug, optP0dev, optPV;
  int optMDarg3;
  int optPotentialArg;
  int optTwoBodyDistance,optScalarDensity, optVectorDensity,optVectorPotential;
  int optPotentialDensity;
  int optRQMDevolution;
  double facMesonPot;
  int optLambdaPot;
  Pythia8::Vec4 pTot0, pTot;
  Pythia8::Vec4 pHat;
  int optRecoverEnergy;
  int optVdot, optBaryonCurrent;
  int optPropagate;
  bool optDerivative;
  bool firstEng;
  static bool firstCall;
  double gamCM, gamCM2;
  double globalTime;
  TwoBodyDistance *distance, *distanceP;
  double Alpha,Beta,Gam,rho0,Vex1,Vex2,pMu1,pMu2;
  int    optCollisionOrdering;
  double lambdaLag;

public:
  MeanField(Pythia8::Settings* s);
  virtual ~MeanField() {delete distance;delete distanceP;}
  virtual void evolution(std::list<EventParticle*>& plist,double t, double dt,int step)=0;
  //virtual void singleParticlePotential()=0;
  virtual void init(std::list<EventParticle*>& plist)=0;
  virtual Pythia8::Vec4 computeEnergy(std::list<EventParticle*>& plist,int step)=0;
  void phat(Vec4 p) {pHat=p;}
  void rescaleForce(Pythia8::Vec4& qdot);

  double lambda(EventParticle* ip) {
      Vec4 p = optPropagate == 0 ? ip->getP() : ip->getPkin();
      if(optCollisionOrdering<100) return 1.0/p[0];
      if(optCollisionOrdering==101) {
        return 1.0/(p*pHat);
      } else if(optCollisionOrdering==104) {
       	return lambdaLag;
      }else{
	double m= ip->getMass();
	if(m ==0.0) m=1.0;
	return 1.0/m;
      }
}

  // convert evolution parameter to the particle time.
  double tau2t(double tau, EventParticle* ip) {
    Vec4 p = optPropagate == 0 ? ip->getP() : ip->getPkin();
    return (tau - ip->tevol()) * (p.e() * lambda(ip) ) + ip->getT();
  }
};

}
#endif
