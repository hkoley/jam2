#ifndef jam2_meanfield_VectorPotential2_h
#define jam2_meanfield_VectorPotential2_h

#include <Pythia8/Settings.h>
#include <jam2/collision/EventParticle.h>
//#include <jam2/meanfield/PotentialType.h>

namespace jam2 {

class VectorPotential2
{
private:
  Pythia8::Settings* settings;
  std::vector<double> bFac, cFac, tFac, pFac;
  double rho0,cutOffPot;
  double vex1,vex2,pmu1,pmu2;
  int nV=4;
  bool withMomDep;
  int optVectorPotential,eosType,transportModel,optPotentialType;
  std::vector<double> Vpot={}, dVdn={};
  //std::array<double,601> Vpot={}, dVdn={};
  double rhoMax=0.0,dRho=0.0,rhoMin=0.0;
  int maxN=0;
  static bool firstCall;
  bool useTable=false;
public:
  VectorPotential2(Pythia8::Settings* s);
  ~VectorPotential2() { };
  int  nv() const {return nV;}
  bool isMomDep() const {return withMomDep;}
  double potcut() const {return cutOffPot;}
  void setPotentialParam(EventParticle* p);
  void setLambdaPotential(double* fp);
  void setSigmaPotential(double* fp);
  std::vector<double> bfac() {return bFac;}
  std::vector<double> cfac() {return cFac;}
  std::vector<double> tfac() {return tFac;}
  std::vector<double> pfac() {return pFac;}
  void readPotentialParam(std::string fn,double mu1,double c1);
  void makeMDPotVDF(double mu1, double c1);
  void makePT();
  bool isPotTable() const {return maxN;}
  double getRhoMin() const {return rhoMin;}

  // VDF get one-praticle potential
  double vVDF(double n) {
    double v=0.0;
    for(int j=0;j<nV;j++)
     v += pFac[j]*tFac[j]*pow(n,bFac[j]);
    return v;
  }
  // get the derivative of the VDF one-praticle potential
  double dvVDF(double n) {
    double dv=0.0;
    for(int j=0;j<nV;j++)
     dv += pFac[j]*tFac[j]*bFac[j]*pow(n,bFac[j]-1);
    return dv;
  }

  double Vrho(std::vector<double>& rhog, double rho) {
    if(useTable) {
      return getV(rho)/rho;
    } else {
      double vv=0.0;
      for(int j=0;j<nV;j++)
       vv += pFac[j]*tFac[j]*rhog[j];  // V/rho_B
      return vv;
    }
  }
  double delV(std::vector<double>& rhog, double rho) {
    if(useTable) {
      return (rho*getdVdn(rho)-getV(rho))/(rho*rho*rho);
    } else {
      double dv=0.0;
      for(int j=0;j<nV;j++)
      dv += (bFac[j]-1.0)*pFac[j]*tFac[j]*rhog[j]/(rho*rho);  // del(V/rho)/rho
      return dv;
    }
  }

  // n in the unit of 1/fm^3
  double getV(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return Vpot[i]*(1.0-x) + Vpot[i+1]*x;
  }
  // n in the unit of 1/fm^3
  double getdVdn(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return dVdn[i]*(1.0-x) + dVdn[i+1]*x;
  }

  double singleParticlePotMD(double p, double pf, double lambda);
  double potMomdepT0(double pf, double lam);
  double dVmdpf(double pf, double lam);
  std::pair<double,double> dVmdn(double rho, double lam, double c);


  double Vmd(double psq1,double vfac1,double vfac2,double pfi1,double pfi2,double pfj1,double pfj2) {
    return vfac1*vex1/(1.0-psq1/(pfi1*pmu1)) + vfac2*vex2/(1.0-psq1/(pfi2*pmu2));
  }
  /*
  void Vmd(double psq1,double psq2,double vfac1,double vfac2,double pfi1,double pfi2,double pfj1,double pfj2)
{
  vmom4i = vfac1*vex1/(1.0-psq1/(pfi1*pmu1)) + vfac2*vex2/(1.0-psq1/(pfi2*pmu2));
  vmom4j = vfac1*vex1/(1.0-psq2/(pfj1*pmu1)) + vfac2*vex2/(1.0-psq2/(pfj2*pmu2));
}
*/

  double devVmd2(double psq,double vf1,double vf2, double pf1,double pf2) {
    return vf1*vex1/(1.0 - psq/(pf1*pmu1)) + vf2*vex2/(1.0 - psq/(pf2*pmu2));
  }
  double devVme2(double psq,double vf1,double vf2,double pf1,double pf2) {
    double fac1 = 1.0 - psq/(pf1*pmu1);
    double fac2 = 1.0 - psq/(pf2*pmu2);
    return -vf1*vex1/(pmu1*fac1*fac1) - vf2*vex2/(pmu2*fac2*fac2);
  }
};

} // end namespace jam2
#endif
