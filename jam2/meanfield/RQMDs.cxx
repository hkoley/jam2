#include <jam2/meanfield/RQMDs.h>

// Relativistic quantum molecular dynamics with Skyrme potential
// potentials are implemented as scalar type.

namespace jam2 {

using namespace std;

bool RQMDs::firstCall=true;

RQMDs::RQMDs(Pythia8::Settings* s) : MeanField(s)
{
  withMomDep=1;
  double alpha=0.0,beta=0.0;
  double C1=0.0, C2=0.0, mu1=1.0, mu2=1.0;
  if(eosType==1) {     // Skyrme Hard Nara2019 K=380 
    //alpha = -124.0e-3;
    //beta = 70.5e-3;
    //gam = 2.0;

    alpha = -0.12384212774318967;
    beta = 0.06875425035612921;
    gam = 2.123648826081624;
    withMomDep=0;

  } else if(eosType==2) { // Skyrme Soft Nara2019 K=210
    //alpha = -356.0e-3;
    //beta = 303.0e-3;
    //gam = 1.16667;

    alpha = -0.2621174235726939;
    beta = 0.20645616373308995;
    gam = 1.2652357572720947;
    withMomDep=0;

  } else if(eosType==3) { // MH2 Skyrme + mom.dep Nara2019 K=380
    alpha = 0.00877512904142221;
    beta = 0.07048243772253593;
    gam = 1.8907272542475995;
    C1 = -0.3907221709140509;
    C2= 0.3341868667966711;
    mu1 = 2.02*HBARC;
    mu2= 1.0*HBARC;

  } else if(eosType==4) { // MH2 Skyrme + mom.dep Nara2019 K=210
    alpha = -0.07785801989125717;
    beta = 0.1565487207865633;
    gam = 1.261622285760052;
    C1 = -0.3907221709140509;
    C2= 0.3341868667966711;
    mu1 = 2.02*HBARC;
    mu2= 1.0*HBARC;

  } else if(eosType==5) { // MH3 Skyrme + mom.dep Nara2019 K=380
    alpha = 0.03911636591740648;
    beta = 0.043064133086746524;
    gam = 2.36458860928333;
    C1 = -0.2048087947472817;
    mu1 = 2.8*HBARC;
    C2= 0.04713369448491099;
    mu2= 1.0*HBARC;

  } else if(eosType==6) { // MS3 Skyrme + mom.dep Nara2019 K=210
    alpha = -0.6285006244768873;
    beta = 0.7070852697952635;
    gam = 1.0496537865787445;
    C1 = -0.20192630016292473;
    C2= 0.05694208245922472;
    mu1 = 2.8*HBARC;
    mu2= 1.0*HBARC;

  } else if(eosType==7) { // MH4 Skyrme + mom.dep Nara2019 K=380
    alpha = 0.045166262659641;
    beta = 0.03824971687635796;
    gam = 2.509835588271163;
    C1 = -0.1787443736909779;
    mu1 = 3.0754425376340975*HBARC;

  } else if(eosType==8) { // MS4 Skyrme + mom.dep Nara2019 K=210
    alpha = -0.0896591573281629;
    beta = 0.17249044531651028;
    gam = 1.202859798739601;
    C1 = -0.1787443736909779;
    mu1 = 3.0754425376340975*HBARC;
 
  //--------------------------------------------------------------------------
  // optical potential is defined by sqrt{(m_N+S)^2+p^2} - sqrt{m_N^2 + p^2}
  // m*/m=0.88
  } else if(eosType==11) { // NH2 Skyrme + mom.dep Nara2021 K=380

    // U( Pinf= 1.7 )= 0.05  Einf= 1.0036086114353737 Pinf= 1.7
    // U( 0.685 )= 0.0 Elab= 0.22349429615474214
    // meff=  0.8823864683629444  M*/M= 0.9407105206427979 rho0= 0.168
    alpha = -0.14746618076876553;
    beta = 0.28231864726575806;
    gam = 1.3086087855271107;
    C1 = -0.8500057544566262;
    mu1 = 2.02*HBARC;
    C2= 1.0508877826863514;
    mu2= 1.0*HBARC;

  } else if(eosType==12) { // MS2 Skyrme + mom.dep Nara2021 K=210
    // U( Pinf= 1.7 )= 0.05  Einf= 1.0036086114353737 Pinf= 1.7
    // U( 0.685 )= 0.0 Elab= 0.22349429615474214
    // meff=  0.8823864683629444  M*/M= 0.9407105206427979 rho0= 0.168
    alpha = -1.7403675255660511;
    beta = 1.8746769579585103;
    gam = 1.0351617222744887;
    C1 = -0.8500057544566262;
    C2= 1.0508877826863514;
    mu1 = 2.02*HBARC;
    mu2= 1.0*HBARC;

  // optical potential is defined by sqrt{(m_N+S)^2+p^2} - sqrt{m_N^2 + p^2}
  // m*/m=0.88
  } else if(eosType==13) { // NH5 Skyrme + mom.dep Nara2019 K=380
    alpha = 0.1436791875727167;
    beta = 0.04604753299756481;
    gam = 2.3300535324726885;
    C1 = -0.272173218332104;
    mu1 = 5.077642517924502;

  } else if(eosType==14) { // Skyrme + mom.dep Ohnishi2015 K=272.59
    alpha = -0.208886959978512;
    beta  = 0.284037556284497;
    gam   = 7.0/6.0;
    mu1 = 2.02*HBARC;
    mu2 = 1.0*HBARC;
    C1  =  -0.38314;
    C2  =   0.33741;

  } else if(eosType==15) { // Skyrme  K=550
    alpha = -0.11096186950393147;
    beta = 0.05645200738039593;
    gam = 2.774173096033283;
    withMomDep=0;

  } else if(eosType==16) { // Skyrme + mom.dep K=450
    alpha = 0.050658464813240926;
    beta = 0.03300384907908334;
    gam = 3.0470954207158307;
    C1 = -0.17874437370225157;
    mu1 = 3.0754425374321817;

  } else if(eosType>=51 && eosType<=54) { // CMF
    makePot= new MakePotential(settings);
    withMomDep=makePot->isMomDep();
    C1  = makePot->getC1();
    C2  = makePot->getC2();
    mu1 = makePot->getMu1();
    mu2 = makePot->getMu2();

  } else {
    cout << "RQMDs mode MeanField:EoStype not implemented " << eosType << endl;
    exit(1);
  }

  t1 = 0.5*alpha/rho0;
  t3 = beta/(gam+1.0)/pow(rho0,gam);
  t3f = gam*t3;

  pmu1= mu1*mu1;
  pmu2= mu2*mu2;
  vex1=C1/(2*rho0);
  vex2=C2/(2*rho0);

  double widG = settings->parm("MeanField:gaussWidth");
  facG = 1.0/pow(4.0*M_PI*widG, 1.5);
  wG = 1.0/(4*widG);

  // this mode does not have m_i/p^0_i term.
  if(optTwoBodyDistance==3) optDerivative=false;

  if(firstCall) {
  cout << "# RQMDs mode eosType= "<< eosType  << " rho0= " << rho0
      << " alpha= " << alpha
      << " beta= "<< beta << " gamma= "<< gam
      << " vex1 = "<< vex1 << " vex2 = "<< vex2
      << " pmu = "<< pmu1 << " pmu2 = "<< pmu2
      <<endl;
    firstCall=false;
    cout <<"# RQMDs t1= "<< t1<< " t3f= "<< t3f <<endl;
  }
   
}

// This is called before starting time evolution.
void RQMDs::init(std::list<EventParticle*>& plist)
{
  firstEng=true;
  return;

}

void RQMDs::initMatrix(std::list<EventParticle*>& plist,double t,int step)
{
  part.clear();
  eFree=0.0;
  pHat2=0.0;
  for(auto& p : plist) {
    //if(optCollisionOrdering>100 && p->tevol() > t ) continue;
    //double tp=p->getT();
    double tp = optCollisionOrdering < 100 ? t : p->tau2t(t,optPropagate);

    //double tcm=pHat*p->getR();
    //double tfcm=pHat*p->propagate(p->getTf(),1);

    /*
    bool isM=p->isMeanField(tp,optPotential);
    //if(isM && tfcm>tcm && p->qFactor()>0) {
    if(!isM ) {
      cout << " t= "<< t
           << " tp= "<< p->getT()
           << " tf= "<< p->getTf()
           << " tcm= "<< tcm
           << " tfcm= "<< tfcm
           << " q= "<< p->qFactor()
           << " id= "<< p->getID()
       	<<endl;
      cin.get();
    }
    */

    //double tauf=p->t2tau(p->getTf(),optPropagate);
    //bool isM=false;
    //if(tauf < t && p->isMeanField(tp,4)) isM=true;

    //if(isM) {
    //if(isM && tfcm>tcm && p->qFactor()>0) {
    if(p->isMeanField(tp,optPotential)) {
      part.push_back(p);
      pHat2 += p->getP();
    } else {
      eFree += p->getP();
    }
  }

  if(step==1) eFree0 = eFree;
  pHat2 /= pHat2.mCalc();

  NV = part.size();

  //std::fill(rho.begin(),rho.end(),0.0);
  //std::fill(vmoms.begin(),vmoms.end(),0.0);

  rho.assign(NV,0.0);
  vmoms.assign(NV,0.0);
  rhog.resize(NV);
  rhom.resize(NV);
  for(int i=0;i<NV;i++) rhom[i].resize(NV);

  force.assign(NV,0.0);
  forcer.assign(NV,0.0);
  lambda.assign(NV,0.0);
}

void RQMDs::clearMatrix()
{
  part.clear();
  rho.clear();
  rhog.clear();
  vmoms.clear();
  for(int i=0;i<NV;i++) rhom[i].clear();
  rhom.clear();
  force.clear();
  forcer.clear();
}



void RQMDs::evolution(list<EventParticle*>& plist, double t, double dt,int step)
{
  globalTime = t;
  initMatrix(plist,t+dt,step);

  if(NV==0) {
    cout << "NV=0? "<<  NV<<endl;
    return;
  }

  // free mass is used in the calculation of potential and force.
  //if(optPotentialArg >= 1) {
    for(int i=0;i<NV;i++) part[i]->setPotS(0.0);
  //}

  Vec4 pHatSave=pHat;
  //pHat = pHat2;

  //qmdMatrix();
  qmdMatrixR();

  bool optSet= optPotentialArg == 0 ? true :  false;
  //singleParticlePotential(optSet);
  singleParticlePotential(false);

  //computeForce();
  computeForceR();

  double dtave=0.0, dtmax=0.0, dtmin=1e+30;

  // update momentum and coordinate.
  for(int i=0; i< NV; i++) {

    Vec4 qdot = forcer[i];
    if(qdot.m2Calc() <=0.0) {
      cout << " forcer^2= "<< qdot.m2Calc()
           << " v= "<< forcer[i].pAbs()/forcer[i][0] << endl;
    }
    double vv=qdot.pAbs()/qdot[0];
    if(vv>=1.0) {
      cout << " v= "<< vv << " v0= "<< qdot[0]
           << " forcer^2= "<< qdot.m2Calc()
           << " tau= "<< t
           << " tp= "<< part[i]->getT()
           << " tf= "<< part[i]->getTf()
           << " q= "<< part[i]->qFactor()
           << " id= "<< part[i]->getID()
       	<<endl;
    }
    dtave += qdot[0];
    dtmax = max(dtmax,qdot[0]);
    dtmin = min(dtmin,qdot[0]);

    double t0=part[i]->getT();
    part[i]->updateByForce(force[i], qdot,dt);
    //if(optCollisionOrdering > 100) {
      part[i]->addT(qdot[0]*dt);
      if(optCollisionOrdering==101) part[i]->tevol(pHat*part[i]->getR());

    //}
    part[i]->updateR(t0, optPropagate);
    if(optCollisionOrdering==101) part[i]->tevol(pHat*part[i]->getR());

    part[i]->lambda(MeanField::lambda(part[i]));

    /*
    cout << "t0= "<< t0 << " t= "<< part[i]->getT()
      << " tau= "<< part[i]->tevol()
      << " tf= "<< part[i]->getTf()
      << " tl= "<< part[i]->TimeLastColl()
	  << " id= "<< part[i]->getID()
	  << " q= "<< part[i]->qFactor()
      <<endl;
      */

  }
  //cout << t  << " dtave= "<<  dtave*dt/NV << " dtmax= "<< dtmax*dt << " dtmin= "<< dtmin*dt <<endl;


  if(!optSet) singleParticlePotential(true);

  if(optCollisionOrdering==101) {
 for(auto& p : part) {
    double lam=p->lambda();
    double lam2 = 1.0/(pHat*p->getPkin());
    if(abs(lam-lam2)>1e-9) {
      cout << scientific << setprecision(16) << " lam= "<< lam << " lam2= "<< lam2 << endl;
      exit(1);
    }
 }
  }

  // Save initial total energy-momentum.
  if(firstEng) computeEnergy(plist,step);

  if(optRecoverEnergy) RecoverEnergy(plist);
  //if(optRecoverEnergy) RecoverEnergy2(plist);

  if(isDebug > 1) computeEnergy(plist,step);

  //clearMatrix();

  pHat = pHatSave;


}

// compute single particle potential energy.
Vec4 RQMDs::computeEnergy(list<EventParticle*>& plist, int step)
{
  pTot=0.0;   
  for(auto i = plist.begin(); i != plist.end(); ++i) {
    pTot += (*i)->getP();
  }

  if(firstEng) {pTot0 = pTot; firstEng=false;}
  if(isDebug > 1) {
  double econ=abs(pTot0[0]-pTot[0])/pTot0[0]*100;
  cout << "RQMDs time= "
      << " econ= " << fixed << econ << " %"
     << scientific << setw(13) << pTot[1] 
     << scientific << setw(13) << pTot[2]
     << scientific << setw(13) << pTot[3]
     <<endl;
  }

  return pTot;

}

void RQMDs::singleParticlePotential(bool optSet)
{
  for(int i=0; i< NV; i++) {
    double pot=0.0;
    if(makePot) {
      pot=makePot->getV(rho[i]);
    } else {
      rhog[i] = pow(max(0.0,rho[i]),gam-1);
      pot=(t1 + t3*rhog[i])*rho[i];
    }
    double vsky = part[i]->baryon()/3*pot;
    part[i]->setPotS(vsky + vmoms[i],optSet);
    part[i]->lambda(MeanField::lambda(part[i]));
  }
}

void RQMDs::qmdMatrix(double a)
{
  for(auto i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    p1 *= a;
    double qfac1=1.0;
    if(optPotential<4) qfac1 = part[i]->getTf() > part[i]->getT() ? part[i]->qFactor() : 1.0;

    distance->setRP1(r1,p1);
    double fi=p1.mCalc()/p1[0];

    for(auto j=i+1; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      p2 *=a;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > part[j]->getT() ? part[j]->qFactor() : 1.0;

      distance->setRP2(r2,p2);
      double fj=p2.mCalc()/p2[0];

      distance->density();
      rhom[i][j] = distance->density1*qfac1*qfac2;
      rhom[j][i] = distance->density2*qfac1*qfac2;

      rho[i] += rhom[i][j]*fj;
      rho[j] += rhom[j][i]*fi;

      if(!withMomDep) continue;

      distanceP->psq();
      double  pmom2ij = vex1/(1.0-distance->psq1/pmu1)+vex2/(1.0-distance->psq1/pmu2);
      double  pmom2ji = vex1/(1.0-distance->psq2/pmu1)+vex2/(1.0-distance->psq2/pmu2);
      vmoms[i] += pmom2ij*rhom[i][j]*fj;
      vmoms[j] += pmom2ji*rhom[j][i]*fi;

    }
  }
}

// QMD matrix for the distance seen from the rest frame of particle j
void RQMDs::qmdMatrixR(double a)
{
  for(auto i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    p1 *= a;
    double m1 = part[i]->getMass();
    Vec4 u1 = p1/m1;
    double qfac1=1.0;
    if(optPotential<4) qfac1 = part[i]->getTf() > part[i]->getT() ? part[i]->qFactor() : 1.0;

    for(auto j=i+1; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      p2 *=a;
      double m2 = part[j]->getMass();
      Vec4 u2 = p2/m2;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > part[j]->getT() ? part[j]->qFactor() : 1.0;

      Vec4 dR = r1 - r2;
      double drsq = dR.m2Calc();
      double drsq1 = drsq - pow2(dR*u2);
      double drsq2 = drsq - pow2(dR*u1);
      rhom[i][j] = facG * exp(drsq1*wG)*qfac1*qfac2;
      rhom[j][i] = facG * exp(drsq2*wG)*qfac1*qfac2;
      rho[i] += rhom[i][j];
      rho[j] += rhom[j][i];

      if(!withMomDep) continue;

      Vec4 dp = p1 - p2;
      double dpsq = dp.m2Calc();
      double psqi = dpsq - pow2(dp * u2);
      double psqj = dpsq - pow2(dp * u1);
      double  pmom2ij = vex1/(1.0-psqi/pmu1)+vex2/(1.0-psqi/pmu2);
      double  pmom2ji = vex1/(1.0-psqj/pmu1)+vex2/(1.0-psqj/pmu2);
      vmoms[i] += pmom2ij*rhom[i][j];
      vmoms[j] += pmom2ji*rhom[j][i];

    }
  }
}

void RQMDs::computeForce()
{
  for(auto i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    double fi = p1.mCalc()/p1[0];
    double meff1 = part[i]->getEffectiveMass();
    Vec4 pk1=part[i]->getPkin();
    double fengi = optCollisionOrdering == 101 ? 1.0/(pk1*pHat) :
            optCollisionOrdering >  101 ? part[i]->getMass()/(pk1*p1) : 1.0/pk1[0];

    lambda[i] += pk1*pHat;
    forcer[i] += pk1*fengi;
    distance->setRP1(r1,p1);
    distanceP->setRP1(r1,p1);

    for(auto j=i+1; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      double fj = p2.mCalc()/p2[0];
      double meff2 = part[j]->getEffectiveMass();
      Vec4 pk2=part[j]->getPkin();
      double fengj = optCollisionOrdering == 101 ? 1.0/(pk2*pHat) :
              optCollisionOrdering  > 101 ? part[j]->getMass()/(pk2*p2) : 1.0/pk2[0];

      distance->setRP2(r2,p2);
      distanceP->setRP2(r2,p2);
      distance->distanceR();

      double dvi=0.0, dvj=0.0;
      if(makePot) {
	dvi = makePot->getdVdn(rho[i]);
	dvj = makePot->getdVdn(rho[j]);
      } else {
        dvi = t1 + t3f*rhog[i];
        dvj = t1 + t3f*rhog[j];
      }
      double fsky1 = -wG*meff1*fengi*dvi*rhom[i][j]*fj; 
      double fsky2 = -wG*meff2*fengj*dvj*rhom[j][i]*fi; 

      force[i]  += -fsky1*distance->dr2ijri - fsky2*distance->dr2jiri;
      force[j]  += -fsky1*distance->dr2ijrj - fsky2*distance->dr2jirj;
      forcer[i] +=  fsky1*distance->dr2ijpi + fsky2*distance->dr2jipi;
      forcer[j] +=  fsky1*distance->dr2ijpj + fsky2*distance->dr2jipj;

      lambda[i] += fsky1/fengi*distance->dr2ijpi*pHat;
      lambda[j] += fsky2/fengj*distance->dr2jipj*pHat;

      if(optDerivative && optP0dev) {
        forcer[i] +=  fsky2*p1/(wG*p1[0]*p1[0]);
        forcer[j] +=  fsky1*p2/(wG*p2[0]*p2[0]);

        // gamma derivative.
        distance->devGamma();
        double facsk= -(fsky1+fsky2)/wG;
        forcer[i] += distance->devgam1*facsk;
        forcer[j] += distance->devgam2*facsk;
      }

      if(!withMomDep) continue;

      distanceP->distanceP();
      double fmomdi = -wG*meff1*fengi*devVmd(distanceP->psq2)*rhom[i][j]*fj;
      double fmomei =     meff1*fengi*devVme(distanceP->psq2)*rhom[i][j]*fj;
      double fmomdj = -wG*meff2*fengj*devVmd(distanceP->psq1)*rhom[j][i]*fi;
      double fmomej =     meff2*fengj*devVme(distanceP->psq1)*rhom[j][i]*fi;

      force[i]  += -fmomdi*distance->dr2ijri - fmomdj*distance->dr2jiri;
      force[j]  += -fmomdi*distance->dr2ijrj - fmomdj*distance->dr2jirj;
      forcer[i] +=  fmomei*distanceP->dp2ijpi + fmomej*distanceP->dp2jipi
	          + fmomdi*distance->dr2ijpi + fmomdj*distance->dr2jipi;
      forcer[j] +=  fmomei*distanceP->dp2ijpj + fmomej*distanceP->dp2jipj
	          + fmomdi*distance->dr2ijpj + fmomdj*distance->dr2jipj;

      lambda[i] +=  (fmomdi*distance->dr2ijpi + fmomei*distanceP->dp2ijpi)*pHat/fengi;
      lambda[j] +=  (fmomdj*distance->dr2jipj + fmomej*distanceP->dp2jipj)*pHat/fengj;

      if(optDerivative) {
        forcer[i] +=  fmomdj*p1/(wG*p1[0]*p1[0])*optP0dev;
        forcer[j] +=  fmomdi*p2/(wG*p2[0]*p2[0])*optP0dev;

        // gamma derivative.
        double facmom = -(fmomdi + fmomdj)/wG;
        forcer[i] += distance->devgam1*facmom;
        forcer[j] += distance->devgam2*facmom;
      }


    }
  }

}

void RQMDs::computeForceR()
{
  vector<std::vector<Vec4> > matp(NV,vector<Vec4>(NV,0.0));
  vector<std::vector<Vec4> > matr(NV,vector<Vec4>(NV,0.0));

  for(auto i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 pk1=part[i]->getPkin();
    double meff1 = part[i]->getEffectiveMass();
    double m1=part[i]->getMass();
    Vec4 u1  = p1/m1;
    double lam1 = optCollisionOrdering == 101 ? 1.0/(pk1*pHat) :
                  //optCollisionOrdering >  101 ? 1.0/(pk1*u1)   :
                  optCollisionOrdering >  101 ? 1.0/pk1.mCalc()   :
		  1.0/pk1[0];

    matp[i][i] += 2*pk1;
    lambda[i]  += pk1*pHat;
    forcer[i]  += pk1*lam1;

    for(auto j=i+1; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      Vec4 pk2=part[j]->getPkin();
      double meff2 = part[j]->getEffectiveMass();
      double m2=part[j]->getMass();
      Vec4 u2  = p2/m2;
      double lam2 = optCollisionOrdering == 101 ? 1.0/(pk2*pHat) :
                    //optCollisionOrdering  > 101 ? 1.0/(pk2*u2)   :
                    optCollisionOrdering  > 101 ? 1.0/pk2.mCalc()  :
		    1.0/pk2[0];

      double dvi=0.0, dvj=0.0;
      if(makePot) {
	dvi = makePot->getdVdn(rho[i]);
	dvj = makePot->getdVdn(rho[j]);
      } else {
        dvi = t1 + t3f*rhog[i];
        dvj = t1 + t3f*rhog[j];
      }
      double fsky1 = wG*meff1*dvi*rhom[i][j];
      double fsky2 = wG*meff2*dvj*rhom[j][i]; 

      Vec4 dr = r1 - r2;
      double rbi = dr*u1;
      double rbj = dr*u2;
      Vec4 dr2ijri =  2*(dr - rbj*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jiri =  2*(dr - rbi*u1);  // R^2_{ji}/dr_i
      Vec4 dr2ijrj =  -dr2ijri;
      Vec4 dr2jirj =  -dr2jiri;

      Vec4 dr2jipi =  2*dr*rbi/m1;     // R^2_{ji}/dp_i
      Vec4 dr2ijpj =  2*dr*rbj/m2;     // R^2_{ij}/dp_j
      Vec4 dr2ijpi = 0.0;
      Vec4 dr2jipj = 0.0;

      force[i]  +=  fsky1*lam1*dr2ijri + fsky2*lam2*dr2jiri;
      force[j]  +=  fsky1*lam1*dr2ijrj + fsky2*lam2*dr2jirj;
      forcer[i] += -fsky1*lam1*dr2ijpi - fsky2*lam2*dr2jipi;
      forcer[j] += -fsky1*lam1*dr2ijpj - fsky2*lam2*dr2jipj;

      lambda[i] += -fsky1*dr2ijpi*pHat;
      lambda[j] += -fsky2*dr2jipj*pHat;

      matp[i][j]  = -2*fsky1*dr2ijpj;
      matp[j][i]  = -2*fsky2*dr2jipi;
      matp[i][i] += -2*fsky1*dr2ijpi;
      matp[j][j] += -2*fsky2*dr2jipj;

      matr[i][j]  =  2*fsky1*dr2ijrj;
      matr[j][i]  =  2*fsky2*dr2jiri;
      matr[i][i] +=  2*fsky1*dr2ijri;
      matr[j][j] +=  2*fsky2*dr2jirj;

      if(!withMomDep) continue;

      Vec4 dp = p1 - p2;
      double psq = dp.m2Calc();
      // distance squared in the rest frame of particle 2
      double dot4j = dp * u2;
      psq2 = psq - dot4j*dot4j;

      // distance squared in the rest frame of particle 1
      double dot4i = dp * u1;
      psq1 = psq - dot4i*dot4i;

      // derivatives
      Vec4 bi = p1/p1.e();
      Vec4 bb = optP0dev * p2.e()*bi - p2;
      dp2ijpi = 2*(dp - optP0dev*dp[0]*bi + bb*dot4j/m2);
      dp2jipi = 2*(dp - optP0dev*dp[0]*bi - bb*dot4i/m1);

      //dp2ijpi = 2*(dp - dot4j*u2);
      //dp2jipi = 2*(dp + dot4i*p2/m1);

      dp2ijpi = 2*(dp + dot4j*u2);
      //dp2jipi = 2*(dp + dot4i*(2*p1-p2)/m1);
      dp2jipi = 2*(dp + dot4i*u1);

      Vec4 bj = p2/p2.e();
      Vec4 bb2 = optP0dev * p1.e()*bj - p1;
      dp2ijpj = 2*(-dp + optP0dev*dp[0]*bj + bb2*dot4j/m2);
      dp2jipj = 2*(-dp + optP0dev*dp[0]*bj - bb2*dot4i/m1);

      //dp2ijpj = 2*(-dp - dot4j*p1/m2);
      //dp2jipj = 2*(-dp + dot4i*u1);

      //dp2ijpj = 2*(-dp - dot4j*(p1-2*p2)/m2);
      dp2ijpj = 2*(-dp + dot4j*u2);
      dp2jipj = 2*(-dp + dot4i*u1);

      double fmomdi = -wG*meff1*devVmd(psq2)*rhom[i][j];
      double fmomei =     meff1*devVme(psq2)*rhom[i][j];
      double fmomdj = -wG*meff2*devVmd(psq1)*rhom[j][i];
      double fmomej =     meff2*devVme(psq1)*rhom[j][i];

      force[i]  += -fmomdi*lam1*dr2ijri - fmomdj*lam2*dr2jiri;
      force[j]  += -fmomdi*lam1*dr2ijrj - fmomdj*lam2*dr2jirj;
      forcer[i] +=  fmomei*lam1*dp2ijpi + fmomej*lam2*dp2jipi
	          + fmomdi*lam1*dr2ijpi + fmomdj*lam2*dr2jipi;
      forcer[j] +=  fmomei*lam1*dp2ijpj + fmomej*lam2*dp2jipj
	          + fmomdi*lam1*dr2ijpj + fmomdj*lam2*dr2jipj;

      lambda[i] +=  (fmomdi*dr2ijpi + fmomei*dp2ijpi)*pHat;
      lambda[j] +=  (fmomdj*dr2jipj + fmomej*dp2jipj)*pHat;

      matp[i][j] +=  2*fmomdi*dr2ijpj + 2*fmomei*dp2ijpj;
      matp[j][i] +=  2*fmomdj*dr2jipi + 2*fmomej*dp2jipi;
      matp[i][i] +=  2*fmomdi*dr2ijpi + 2*fmomei*dp2ijpi;
      matp[j][j] +=  2*fmomdj*dr2jipj + 2*fmomej*dp2jipj;

      matr[i][j] += -2*fmomdi*dr2ijrj;
      matr[j][i] += -2*fmomdj*dr2jiri;
      matr[i][i] += -2*fmomdi*dr2ijri;
      matr[j][j] += -2*fmomdj*dr2jirj;

    }
  }

  if(optRQMDevolution<=1) {
    for(auto& fr:forcer) {
      if(fr.m2Calc()<=0.0) rescaleForce(fr);
      //if(optCollisionOrdering< 100) forcer[0]=1.0;
    }
    return;
  }

  vector<double> lambdaw(NV-1);

  // take only the diagonal part.
  if(optRQMDevolution==2) {
    if(optCollisionOrdering  == 101) {
      for(int i=0; i< NV; i++) lambda[i]=1.0/(pHat*matp[i][i]);
    } else {
      for(int i=0; i< NV; i++) {
	double m=matp[i][i].mCalc();
	if(!m) {
	  cout << " m= "<< m <<endl;
	  exit(1);
	}
	lambda[i]=1.0/m;
	//cout << "lam= "<< lambda[i]<<endl;
      }
    }

  // full solution.
  } else if(optRQMDevolution==3 || optRQMDevolution==4) {
    vector<std::vector<double> > mat(NV,vector<double>(NV+1,1.0));
    for(int i=0; i< NV; i++) 
    for(int j=0; j< NV; j++) {
      mat[i][j]=matp[j][i]*pHat;
    }
    findLagrangeMultiplier(mat,NV);
    for(int i=0;i<NV;i++) lambda[i]=mat[i][NV];

    // do not assume H_i's are first class [H_i,H_j]=0.
    if(optRQMDevolution==4) {
      vector<std::vector<double> > hh(NV,vector<double>(NV,0.0));
      double hhmax=-1.0;
      for(int i=0; i< NV; i++)
      for(int j=i+1; j< NV; j++) {
      for(int k=0;k<NV;k++) {
        hh[i][j] += matp[i][k]*matr[j][k] - matr[i][k]*matp[j][k];
      }
      hh[j][i]=-hh[i][j];
      hhmax=max(hhmax,abs(hh[i][j]));
      }

      // Joseph Samuel, PRD26 (1982)3475.
      for(int i=0;i<NV-1;i++) {
	lambdaw[i]=0.0;
        for(int j=0;j<NV-1;j++)
  	  lambdaw[i] += lambda[i]*lambda[j]*hh[i][j];
      }
    }

  }

  force.assign(NV,0.0);
  forcer.assign(NV,0.0);
  double pm=pHat.mCalc();
  for(int i=0; i< NV; i++) {
    Vec4 q1 = part[i]->getR();
    if(optRQMDevolution==4) {
      force[i] -= (lambdaw[i]-lambdaw[NV-2])*pHat;
    }
    for(int j=0; j< NV; j++) {
      force[i]  += lambda[j]*matr[j][i];
      forcer[i] += lambda[j]*matp[j][i];
    }
    if(optRQMDevolution==4) {
      for(int j=0; j< NV-1; j++) {
        Vec4 q12 = q1 - part[j]->getR();
        forcer[i]  += lambdaw[j]*(q12 - (pHat*q12)*pHat)/pm;
      }
    }
    if(forcer[i].m2Calc()<=0.0) rescaleForce(forcer[i]);
  }

}

void RQMDs::findLagrangeMultiplier(vector<vector<double> >& mat,int nv)
{
  for (int k=0; k<nv; k++) {

    // Initialize maximum value and index for pivot
    //int i_max = k;
    //int v_max = mat[i_max][k];

    // find greater amplitude for pivot if any
    //for (int i = k+1; i < N; i++)
    //  if (abs(mat[i][k]) > v_max) v_max = mat[i][k], i_max = i;

    //if (!mat[k][i_max]) {
    //  cout << " singlular matirx"<<endl;
    //  exit(1);
    //}
    if (!mat[k][k]) {
      cout << " singlular matirx "<< mat[k][k] << endl;
      exit(1);
    }

   // Swap the greatest value row with current row.
   //if (i_max != k) swap_row(mat, k, i_max);

    for (int i=k+1; i<nv; i++) {
      // factor f to set current row kth element to 0,and subsequently remaining kth column to 0.
      double f = mat[i][k]/mat[k][k];

      // subtract fth multiple of corresponding kth row element
      for (int j=k+1; j<=nv; j++) mat[i][j] -= mat[k][j]*f;

     // filling lower triangular matrix with zeros
     mat[i][k] = 0;
    }
  }

  // Start calculating from last equation up to the first.
  for (int i = nv-1; i >= 0; i--) {
    // start with the RHS of the equation
    //lambda[i] = mat[i][nv];

    // Initialize j to i+1 since matrix is upper. triangular.
    for (int j=i+1; j<nv; j++) {
    // subtract all the lhs values except the coefficient of the variable whose value is being calculated
      //lambda[i] -= mat[i][j]*lambda[j];
      mat[i][nv] -= mat[i][j]*mat[j][nv];
    }

    // divide the RHS by the coefficient of the unknown being calculated
    //lambda[i] = lambda[i]/mat[i][i];
    mat[i][nv] /= mat[i][i];
  }

}

double RQMDs::funcEnergy(list<EventParticle*>& plist,double a)
{
  std::fill(rho.begin(),rho.end(),0.0);
  std::fill(vmoms.begin(),vmoms.end(),0.0);
  qmdMatrix(a);
  bool optSet= optPotentialArg == 0 ? true :  false;
  singleParticlePotential(optSet);
  double etot=0.0;
  for(auto i = plist.begin(); i != plist.end(); ++i) {
    double m = (*i)->getMass() + (*i)->pots();
    double pa2 = (*i)->pAbs2();
    double e = sqrt(m*m + a*a*pa2);
    etot += e;
  }
  return etot;
}

void RQMDs::RecoverEnergy(list<EventParticle*>& plist)
{
  // total energy of the system 
  Vec4 ptot=pTot+eFree0;
  double etot0 = ptot[0];

  Vec4 ptotcm=0.0;
  bool boost = false;
  if(ptot.pAbs2() > 1e-9)  {
    boost = true;
    etot0 = ptot.mCalc();
    for(auto i = plist.begin(); i != plist.end(); ++i) ptotcm += (*i)->getP();
    Vec4 ptot4 = 0.0;
    for(auto i = plist.begin(); i != plist.end(); ++i) {
	(*i)->bstback(ptotcm);
	ptot4 += (*i)->getP();
    }
    cout << "RQMDs::RecoverEnergy  ptot4= "<< ptot4 ;
    cout  << " e= "<< etot0 << endl;
    exit(1);
  }

  /*
  double a0=1.0;
  double a1=1.01;
  double f0 = funcEnergy(plist,a0)-etot0;
  double f1 = funcEnergy(plist,a1)-etot0;
  if(f0*f1 > 0.0) {
    cout << " bisec in RQMDs f0= "<< f0 << " f1= "<< f1 <<endl;
    exit(1);
  }
  cout << "f0= "<< f0 << " f1= "<< f1 <<endl;
  cout << " etot0= "<< pTot0[0] << " efree0= "<< eFree0<<endl;
  int itry=0;
  double a;
  double f;
  do {
    a = 0.5*(a0 + a1);
    f = funcEnergy(plist,a) - etot0;
    if(f*f1 > 0.0) {a1 = a; }
    else {a0 = a; }
    if(++itry > 50) {
	cout << " RQMDs bisec does not converge"<<endl;
	exit(1);
    }
    cout << scientific << " a0= "<< a0 << " a1= "<< a1 << " f= "<< f <<endl;
  } while (abs(f) > 1e-5);

  for(auto i = plist.begin(); i != plist.end(); ++i) {
    (*i)->multP(a);
    (*i)->setOnShell();
  }

  cout << " etot0= "<< etot0 << " dif = "<< f <<endl;
  cin.get();
  */


  double a=1.0;
  double etot = funcEnergy(plist,a);
  int itry=0;
  while(abs(etot-etot0) > etot0*1e-3) {
    a = etot0/etot*a;
    etot = funcEnergy(plist,a);
    if(++itry>10) break;
  }
  
  double efinal=0.0;
  for(auto i = plist.begin(); i != plist.end(); ++i) {
    (*i)->multP(a);
    (*i)->setOnShell();
    efinal += (*i)->getPe();
  }

  if(boost) {
    for(auto i = plist.begin(); i != plist.end(); ++i) (*i)->bst(ptotcm);
  }

}

void RQMDs::RecoverEnergy2(list<EventParticle*>& plist)
{
  // total energy of the system 
  Vec4 ptot=pTot+eFree0;
  double etot0 = ptot[0];

  Vec4 ptotcm=0.0;
  bool boost = false;
  if(ptot.pAbs2() > 1e-9)  {
    boost = true;
    etot0 = ptot.mCalc();
    for(auto i = plist.begin(); i != plist.end(); ++i) ptotcm += (*i)->getP();
    Vec4 ptot4 = 0.0;
    for(auto i = plist.begin(); i != plist.end(); ++i) {
	(*i)->bstback(ptotcm);
	ptot4 += (*i)->getP();
    }
    cout << "RQMDs::RecoverEnergy2 Not implemented yet  ptot4= "<< ptot4 ;
    cout  << " e= "<< etot0 << endl;
    exit(1);
  }

  double a=1.0;
  double etot = funcEnergy(plist,a);
  int itry=0;
  while(abs(etot-etot0) > 1e-5) {
    a = etot0/etot*a;

    for(auto i = plist.begin(); i != plist.end(); ++i) {
      (*i)->multP(a);
      (*i)->setOnShell();
    }

    etot = funcEnergy(plist,1.0);
    if(++itry>10) break;
    cout << " a = "<< a << " etot0= " << etot0
     << " etot= "<< etot
     << scientific << " dif = "<< etot-etot0<<endl;
  }
  
  cout << " a = "<< a << " etot0= " << etot0 << " dif = "<< etot-etot0<<endl;

  double efinal=0.0;
  for(auto i = plist.begin(); i != plist.end(); ++i) {
    (*i)->multP(a);
    (*i)->setOnShell();
    efinal += (*i)->getPe();
  }

  cout << "a= "<< a << " ediff = "<< etot0-efinal <<endl;
  cin.get();

  if(boost) {
    for(auto i = plist.begin(); i != plist.end(); ++i) (*i)->bst(ptotcm);
  }

}

// relative distance and its derivatives between p1 and p2
void RQMDs::distanceP1(const Vec4& p1,const Vec4& p2)
{
  Vec4 dp = p1 - p2;
  psq1 = -dp.pAbs2() + (1-optPV) * dp[0]*dp[0];
  psq2 = psq1;
  dp2ijpi = 2*( dp -optP0dev*(1-optPV)*dp[0]*p1/p1.e());
  dp2ijpj = 2*(-dp -optP0dev*(1-optPV)*dp[0]*p2/p2.e());
  dp2jipi = dp2ijpi;
  dp2jipj = dp2ijpj;
}

// relative distance and its derivatives between p1 and p2
// in the two body CM frame.
void RQMDs::distanceP2(const Vec4& p1,const Vec4& p2)
{
  Vec4 dp  = p1 - p2;
  Vec4 pcm = p1 + p2;
  double s = pcm.m2Calc();
  //double pma = pow2(m1*m1 - m2*m2)/s;
  double pma = pow2(dp * pcm)/s;
  psq1 = dp.m2Calc() - optPV * pma;
  psq2 = psq1;
  Vec4 b1 = p1/p1.e();
  Vec4 b2 = p2/p2.e();
  Vec4 bbi = pcm/pcm[0] - optP0dev*b1;
  Vec4 bbj = pcm/pcm[0] - optP0dev*b2;
  dp2ijpi = 2*(dp - optP0dev*dp[0]*b1 + optPV * pcm[0]/s*pma*bbi);
  dp2ijpj = 2*(-dp + optP0dev*dp[0]*b2 + optPV * pcm[0]/s*pma*bbj);
  dp2jipi = dp2ijpi;
  dp2jipj = dp2ijpj;
}

// relative distance squared and its derivatives between p1 and p2
// in the rest frame of p1 or p2.
void RQMDs::distanceP3(const Vec4& p1,const Vec4& p2)
{
  Vec4 dp = p1 - p2;
  double psq = dp.m2Calc();

  // distance squared in the rest frame of particle 2
  double m2 = p2.mCalc();
  double dot4j = dp * p2 / m2;
  psq2 = psq - optPV*dot4j*dot4j;

  // distance squared in the rest frame of particle 1
  double m1 = p1.mCalc();
  double dot4i = (dp * p1)/m1;
  psq1 = psq - optPV*dot4i*dot4i;

  // derivatives
  Vec4 bi = p1/p1.e();
  Vec4 bb = optP0dev * p2.e()*bi - p2;
  dp2ijpi = 2*(dp - optP0dev*dp[0]*bi + optPV*bb*dot4j/m2);
  dp2jipi = 2*(dp - optP0dev*dp[0]*bi - optPV*bb*dot4i/m1);

  Vec4 bj = p2/p2.e();
  Vec4 bb2 = optP0dev * p1.e()*bj - p1;
  dp2ijpj = 2*(-dp + optP0dev*dp[0]*bj + optPV*bb2*dot4j/m2);
  dp2jipj = 2*(-dp + optP0dev*dp[0]*bj - optPV*bb2*dot4i/m1);
}


} // namespace jam2


