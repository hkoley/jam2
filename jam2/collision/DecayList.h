#ifndef jam2_collision_DecayList_H
#define jam2_collision_DecayList_H

#include <ostream>
#include <list>
#include <jam2/collision/InterList.h>

namespace jam2 {

//class EventParticle;

class DecayList : public InterList
{
private:
    //std::list<EventParticle*>::iterator decayer;
    //EventParticle* decayer;
public:
    DecayList() { }
    DecayList(EventParticle* i1,double td) {
      scatt[0] = i1;
      scatt[1] = nullptr;
      collisionOrderTime = td;    // decay evolution time 
      collTime[0]=i1->lifetime(); // decay of particle time
      collTime[1]=collTime[0];
      collType=0;
    }
    int getNumberOfInComing() const {return 1;}
    Pythia8::Vec4 getTotalMom() {return scatt[0]->getP();}
    void   print(std::ostream& os=std::cout) const;

};

}
#endif
