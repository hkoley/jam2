#ifndef jam2_collision_CascCell_h
#define jam2_collision_CascCell_h

#include <vector>
#include <Pythia8/Basics.h>
#include <Pythia8/Settings.h>
#include <jam2/collision/CascBox.h>
#include <jam2/initcond/InitialCondition.h>
#include <jam2/collision/InterList.h>

namespace jam2 {

class CascCell
{
private:
  Pythia8::Settings* settings;
  int optInitCell;
  int optBoxBoundary;
  std::vector<CascBox*> cascBox;
  int maxX, maxY, maxZ, maxN;
  double dX, dY, dZ;
  double xMin, yMin, zMin, xMax, yMax, zMax;
  double expansionStartTime;
  double vX, vY, vZ;
  int optEta;
  int     withBox;
  double  xBox,yBox,zBox;

public:
  CascCell(Pythia8::Settings* s);
  ~CascCell();
  void clear();
  void init(const InitialCondition* initc);

  static const double infLength;

  std::vector<CascBox*>& boxes() {return cascBox;}
  void addNeighbor3(CascBox* box,int ie);
  double wallCollisionTime(const CascBox& box, const Vec4& r, const Vec4& v);
  double wallCollisionTimeEta(const CascBox& box, const Vec4& r, const Vec4& v);
  double staticWallCollisionTime(CascBox& box, Vec4& r, Vec4& v);
  int numberOfCollision();
  InterList* findNextCollision();

  //int site(int x, int y, int z) {return x + maxX*(y+maxY*z);}

  CascBox* box(const Vec4& r) const {
    double tw = std::max(0.0, r.e() - expansionStartTime);
    int ix = std::min(std::max(0,int(floor(r.px()/(dX+vX*tw)) + maxX/2)),maxX-1);
    int iy = std::min(std::max(0,int(floor(r.py()/(dY+vY*tw)) + maxY/2)),maxY-1);
    double dz = optEta==0 ? r.pz()/(dZ+vZ*tw) : r.rap()/dZ;
    int iz = std::min(std::max(0,int(floor(dz) + maxZ/2)),maxZ-1);
    //int iz = std::min(std::max(0,int(floor(r.pz()/(dZ+vZ*tw)) + maxZ/2)),maxZ-1);
    return cascBox[ix + maxX*(iy+maxY*iz)];
  }

  int inside(const Vec4& r) {
    double tw = std::max(0.0, r.e() - expansionStartTime);
    //int ix = std::min(std::max(0,int(floor((r.px()-xMin)/(dX+vX*tw)))),maxX-1);
    //int iy = std::min(std::max(0,int(floor((r.py()-yMin)/(dY+vY*tw)))),maxY-1);
    //int iz = std::min(std::max(0,int(floor((r.pz()-zMin)/(dZ+vZ*tw)))),maxZ-1);
    int ix = std::min(std::max(0,int(floor(r.px()/(dX+vX*tw)) + maxX/2)),maxX-1);
    int iy = std::min(std::max(0,int(floor(r.py()/(dY+vY*tw)) + maxY/2)),maxY-1);
    double dz = optEta==0 ? r.pz()/(dZ+vZ*tw) : r.rap()/dZ;
    int iz = std::min(std::max(0,int(floor(dz) + maxZ/2)),maxZ-1);
    return  ix + maxX*(iy+maxY*iz);
    //return site(ix,iy,iz);
  }

  int boxPosition(const Vec4& r) {
    double tw = std::max(0.0, r.e() - expansionStartTime);
    int ix = int(floor(r.px()/(dX+vX*tw)) + maxX/2);
    int iy = int(floor(r.py()/(dY+vY*tw)) + maxY/2);
    double dz = optEta==0 ? r.pz()/(dZ+vZ*tw) : r.rap()/dZ;
    int iz = int(floor(dz) + maxZ/2);
    if(ix<0 || ix>=maxX) return -1;
    if(iy<0 || iy>=maxY) return -1;
    if(iz<0 || iz>=maxZ) return -1;
    return ix + maxX*(iy+maxY*iz);
  }
   
  double volume(const double t,const CascBox& box) {
    double dt =std::max(0.0, t-expansionStartTime);
    double dz = optEta==0 ? (dZ+vZ*dt) :dt*box.vzvol();
    return (dX+vX*dt)*(dY+vY*dt)*dz;
  }

};
}
#endif
