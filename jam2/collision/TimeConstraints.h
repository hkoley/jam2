#ifndef jam2_collision_TimeConstraints_h
#define jam2_collision_TimeConstraints_h

#include <vector>
#include <list>
#include <array>
#include <memory>
#include <algorithm>
#include <Pythia8/Settings.h>
#include <jam2/collision/EventParticle.h>

namespace jam2 {

class TimeConstraints
{
private:
  Pythia8::Settings* settings;
  std::list<EventParticle*> particles={};
  int optCollisionOrdering;
  int optSeparable;
  Pythia8::Vec4 pHat,pHatC;
  double aSmall, cWidth;
  int optGij=1;
  int optG;
  int maxIt;
  static const double epsG;
  Pythia8::vector<double> chi;
  vector<std::vector<double> > mat;
  std::vector<Pythia8::Vec4> rSave;

public:
  TimeConstraints(Pythia8::Settings* s);
  ~TimeConstraints();

  void setConstraints(double tau,const Pythia8::Vec4& phat);
  void setTimeConstraints(double tau);
  void solveConstraints(double tau);
  double makeConstraints(double tau);
  double makeConstraints2(double tau);
  Pythia8::Vec4 phat() const {return pHat;}

  void setParticle(std::list<EventParticle*>& p)  {particles=p;}
  std::list<EventParticle*>& getParticles()  {return particles;}
  void swap_row(std::vector<std::vector<double> > m, int N, int i, int j);

};

} // end namespace jam2
#endif
