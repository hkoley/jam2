#include <jam2/collision/TimeConstraints.h>
#include <Pythia8/Basics.h>
#include <jam2/hadrons/JamStdlib.h>

//#include <algorithm>

namespace jam2 {

const double TimeConstraints::epsG=1e-9;
//const double TimeConstraints::epsG=0.0;

TimeConstraints::TimeConstraints(Pythia8::Settings* s): settings(s) 
{
  optCollisionOrdering = settings->mode("Cascade:optCollisionOrder");

  maxIt=settings->mode("Cascade:timeFixMaxIteration");
  optGij=settings->mode("Cascade:optGij");
  cWidth=settings->parm("Cascade:gaussWidth");
  optSeparable=settings->mode("Cascade:optSeparable");
  aSmall=0.01;
  aSmall=0.0;

}

TimeConstraints:: ~TimeConstraints()
{
}

void TimeConstraints::setConstraints(double tau, const Vec4& phat)
{
  if(particles.size()==0) return;

  // projector for the overall center-of-mass system (not this cluster.)
  pHat = phat;

  // projector for this cluster system.
  pHatC=0.0;
  for(const auto& p : particles) pHatC += p->getP();
  pHatC /= pHatC.mCalc();

  // Sorge's time constraints
  setTimeConstraints(tau);
  solveConstraints(tau);

}

void TimeConstraints::setTimeConstraints(double tau)
{
  rSave.clear();
  for(const auto& p:particles) {
    rSave.push_back(p->getR());
  }

  optG=0;
  double eps=1e+10,eps1=1e+10;
  bool isback=false;
  for(int it=0;it<maxIt;it++) {
    eps1 = makeConstraints(tau);
    //eps1 = makeConstraints2(tau);
   //cout << it << " tau= "<< tau  << " TimeConstraints::setTimeConstraints eps= "<< eps1 << " nv= "<< particles.size() <<endl;
    /*
    if(eps1 > eps || isinf(eps1)) {
      isback=true;
      //optG=1; 
      break;
    }
    */

    eps=eps1;
    if(eps1 < 1e-5) break;
  } // loop over iteration

  if(eps > 1e-5 && particles.size()>2) 
  cout << "TimeConstraints::setTimeConstrains fails eps= " << eps1 << " at tau= "<< tau << " NV= "<< particles.size() <<endl;

  /*
  int i=0;
  for(auto p:particles) {
    double t1=p->getT();
    double dt = t1 - rSave[i][0];
    cout << "dt= " << dt<<endl;
      i++;
  }
  cin.get();
  */

  if(isback) {
    int i=0;
    for(auto p:particles) {
      p->setT(rSave[i][0]);
      i++;
    }
  }

}

// function for elementary operation of swapping two rows
void TimeConstraints::swap_row(vector<std::vector<double> > m, int N, int i, int j)
{
  for (int k=0; k<=N; k++) {
    double temp = m[i][k];
    m[i][k] = m[j][k];
    m[j][k] = temp;
  }
}

double TimeConstraints::makeConstraints(double tau)
{
  int NV=particles.size();
  if(NV==1) {
    EventParticle *p = particles.front();
    p->setT((dot3(p->getR(),pHat)+tau)/p->getPe());
    return 0.0;
  } else if(NV==2) {
    EventParticle *p1 = particles.front();
    EventParticle *p2 = particles.back();
    Vec4 u=p1->getP() + p2->getP();
    u /= u.mCalc();
    p1->setT( (tau+dot3(p1->getR(),u))/u[0] );
    p2->setT( (tau+dot3(p2->getR(),u))/u[0] );
    return 0.0;
  }

  chi.assign(NV,0.0);
  mat.assign(NV,vector<double>(NV+1,0.0));
  //vector<std::vector<double> > mat(NV,vector<double>(NV+1,0.0));

  Vec4 R=0.0;
  int i1=0;
  auto pend=--particles.end();
  for(auto pa1=particles.begin(); pa1 != particles.end(); ++pa1,++i1) {
    Vec4 r1=(*pa1)->getR();
    Vec4 p1=(*pa1)->getP();
    R += r1;
    chi[i1] += aSmall*((pHat*r1) - tau);
    mat[i1][i1] += aSmall*pHat[0];
    int i2=NV-1;
    for(auto pa2=pend; pa2 !=pa1; --pa2,--i2) {
      Vec4 r2=(*pa2)->getR();
      Vec4 p2=(*pa2)->getP();
      Vec4 r12=r1-r2;
      double q12=r12*r12;
      //if(q12 > distMinSq) continue;
      if(q12 > 0.0) continue;
      Vec4 u12=p1+p2;
      u12 /= u12.mCalc();
      double ur12=u12*r12;
      double G12=1.0, dq0=u12[0];
      double qt12 = (q12 - ur12*ur12)/(4*cWidth);
      Vec4 uij = u12;
      double urij=ur12;
      if(optG==1) {
        r12=rSave[i1]-rSave[i2];
	ur12=u12*r12;
        qt12 = (r12*r12 - ur12*ur12)/(4*cWidth);
      }
      q12 /= (4*cWidth);
      if(optGij==1) {
        G12= exp(q12)/abs(q12-epsG);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12*(1.-1./(q12-epsG))+uij[0]);
      } else if(optGij==2) {
        G12= exp(q12)/(q12-epsG);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12*(1.-1./(q12-epsG))+uij[0]);
      } else if(optGij==3) {
        G12= exp(q12);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12+uij[0]);
      } else if(optGij==4 || optGij==5) {
        G12= optGij==1 ? exp(qt12)/(qt12-epsG) : exp(qt12)/abs(qt12-epsG);
        double q0 = r12[0] - ur12*u12[0];
        dq0=G12*(2*q0/(4*cWidth)*ur12*(1.-1./(qt12-epsG))+uij[0]);
      } else if(optGij==6) {
        G12= exp(qt12);
        double q0 = r12[0] - ur12*u12[0];
        dq0=G12*(2*q0/(4*cWidth)*ur12+uij[0]);
      }
      chi[i1]  += G12*urij;
      chi[i2]  -= G12*urij;
      mat[i1][i1] += dq0;
      mat[i2][i2] += dq0;
      mat[i1][i2] = -dq0;
      mat[i2][i1] = -dq0;
      if(isnan(chi[i1]) || isnan(chi[i2]) || isnan(dq0)) {
	cout << " chi1= " << chi[i1] << " chi2= "<< chi[i2]
	  << " G12= "<< G12 << " urij= "<< urij
	  << " qt12= " << qt12
	  << " q12= " << q12
	  <<endl;
	cout <<"r1= "<< r1;
	cout <<"r2= "<< r2;
	cout <<"p1= "<< p1;
	cout <<"p2= "<< p2;
	exit(1);
      }
    }
  }

  Vec4 Uc = optSeparable == 1 ? pHat : pHatC;
  chi[NV-1]=(Uc*R)/NV - tau;
  mat[NV-1][NV]= -chi[NV-1];

  double eps=0.0;
  for(int k=0;k<NV;k++) {
    //if(abs(mat[k][k])<1e-15) mat[k][k]=1.0;
    mat[NV-1][k]=Uc[0]/NV;
    mat[k][NV] = -chi[k];
    eps += abs(chi[k]);
    //cout << k << scientific << " chi= "<< chi[k] << " mat= "<< mat[k][k] << endl;
  }

  if(isinf(eps)) {
    i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    cout<< " r= "<< (*pa)->getR();
  }
  for(int i=0;i<NV;i++) if(isinf(chi[i])) cout << " chi= "<< chi[i]<<endl;
  cout<<"TimeConstraints::makeConstraints eps? "<< eps <<endl;
  return 1e+10;
  }

      for(int i=0;i<NV;i++)
      for(int j=0;i<NV;i++)
	cout << "mat= "<< mat[i][j]<<endl;
      for(auto pa:particles) {
	cout << " r= "<< pa->getR()
	<< " p= "<< pa->getP();
      }

  // fix time by Newton method.
  for (int k=0; k<NV; k++) {

    // Initialize maximum value and index for pivot
    //int i_max = k;
    //double v_max = mat[i_max][k];
    // find greater amplitude for pivot if any
    //for (int i = k+1; i < NV; i++)
      //if (abs(mat[i][k]) > v_max) v_max = mat[i][k], i_max = i;

    //if (!mat[k][i_max]) {
    if (!mat[k][k]) {
      cout << "TimeConstraints::makeConstraints singlular matirx "<< mat[k][k] << " at k= "<< k
       	<< " nc= "<< NV << " tau= "<< tau << endl;
      mat[k][k]=1.0;
      for(int i=0;i<NV;i++)
      for(int j=0;i<NV;i++)
	cout << "mat= "<< mat[i][j]<<endl;
      cin.get();
    }

   // Swap the greatest value row with current row.
   //if (i_max != k) swap_row(mat,NV, k, i_max);

    for (int i=k+1; i<NV; i++) {
      double f = mat[i][k]/mat[k][k];
      for (int j=k+1; j<=NV; j++) mat[i][j] -= mat[k][j]*f;
      mat[i][k] = 0;
    }
  }
  double dtt=0.0;
  // Start calculating from last equation up to the first.
  for (int i = NV-1; i >= 0; i--) {
    for (int j=i+1; j<NV; j++) {
      mat[i][NV] -= mat[i][j]*mat[j][NV];
    }
    mat[i][NV] /= mat[i][i];
    dtt += abs(mat[i][NV]);
  }
  double alpha=1.0;
  dtt /= NV;
  if(dtt > 0.6) {
    alpha = 0.6/dtt;
  }
  //cout << " dtt= "<< dtt << " tau= "<< tau << " alpha= "<< alpha <<endl;

  i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    (*pa)->addT(alpha * mat[i1][NV]);
    if(isnan(mat[i1][NV])) {
    cout << " dt= "<< mat[i1][NV]<<endl;
    exit(1);
    }


  }

  return eps/NV;
}

// change |r| to satisfy the time constraints.
double TimeConstraints::makeConstraints2(double tau)
{
  int NV=particles.size();
  if(NV==1) {
    EventParticle *p = particles.front();
    p->setT((dot3(p->getR(),pHat)+tau)/p->getPe());
    return 0.0;
  } else if(NV==2) {
    EventParticle *p1 = particles.front();
    EventParticle *p2 = particles.back();
    Vec4 u=p1->getP() + p2->getP();
    u /= u.mCalc();
    p1->setT( (tau+dot3(p1->getR(),u))/u[0] );
    p2->setT( (tau+dot3(p2->getR(),u))/u[0] );
    return 0.0;
  }

  chi.assign(NV,0.0);
  mat.assign(NV,vector<double>(NV+1,0.0));
  //vector<std::vector<double> > mat(NV,vector<double>(NV+1,0.0));

  Vec4 R=0.0;
  int i1=0;
  auto pend=--particles.end();
  for(auto pa1=particles.begin(); pa1 != particles.end(); ++pa1,++i1) {
    Vec4 r1=(*pa1)->getR();
    Vec4 p1=(*pa1)->getP();
    R += r1;
    chi[i1] += aSmall*((pHat*r1) - tau);
    mat[i1][i1] += aSmall*pHat[0];
    int i2=NV-1;
    for(auto pa2=pend; pa2 !=pa1; --pa2,--i2) {
      Vec4 r2=(*pa2)->getR();
      Vec4 p2=(*pa2)->getP();
      Vec4 r12=r1-r2;
      Vec4 u12=p1+p2;
      u12 /= u12.mCalc();
      double ur12=u12*r12;
      double G12=1.0, dq0=u12[0];
      double qt12 = (r12*r12 - ur12*ur12)/(4*cWidth);
      Vec4 uij = u12;
      double urij=ur12;

      if(optG==1) {
        r12=rSave[i1]-rSave[i2];
	ur12=u12*r12;
        qt12 = (r12*r12 - ur12*ur12)/(4*cWidth);
      }
      double q12=r12*r12;
      //if(q12 > distMinSq) continue;
      if(q12 > 0.0) continue;
      q12 /= (4*cWidth);
      double qabs=r12.pAbs();
      Vec4  qhat = r12/qabs;
      qhat[0]=0.0;
      if(optGij==1) {
        G12= exp(q12)/abs(q12-epsG);
        double q0 = 2*qabs;
        dq0=G12*(q0/(4*cWidth)*ur12*(1.-1./(q12-epsG))+uij*qhat);
      } else if(optGij==2) {
        G12= exp(q12)/(q12-epsG);
        double q0 = 2*qabs;
        dq0=G12*(q0/(4*cWidth)*ur12*(1.-1./(q12-epsG))+uij*qhat);
      } else if(optGij==3) {
        G12= exp(q12);
        double q0 = 2*qabs;
        dq0=G12*(q0/(4*cWidth)*ur12+uij*qhat);
      } else if(optGij==4 || optGij==5) {
          G12= optGij==1 ? exp(qt12)/(qt12-epsG) : exp(qt12)/abs(qt12-epsG);
          double q0 = 2*qabs - ur12*(qhat*u12);
          dq0=G12*(q0/(4*cWidth)*ur12*(1.-1./(qt12-epsG))+uij*qhat);
      } else if(optGij==6) {
        G12= exp(qt12);
        double q0 = 2*qabs - ur12*(qhat*u12);
        dq0=G12*(q0/(4*cWidth)*ur12+uij*qhat);
      }
      chi[i1]  += G12*urij;
      chi[i2]  -= G12*urij;
      mat[i1][i1] += dq0;
      mat[i2][i2] += dq0;
      mat[i1][i2] = -dq0;
      mat[i2][i1] = -dq0;
    }
  }

  Vec4 Uc = optSeparable == 1 ? pHat : pHatC;
  chi[NV-1]=(Uc*R)/NV - tau;
  mat[NV-1][NV]= -chi[NV-1];

  double eps=0.0;
  i1=0;
  for(auto pa1=particles.begin(); pa1 != particles.end(); ++pa1,++i1) {
  //for(int k=0;k<NV;k++) {
    if(abs(mat[i1][i1])<1e-15) mat[i1][i1]=1.0;
      Vec4  qhat = (*pa1)->getR();
      qhat /= qhat.pAbs();
      qhat[0]=0.0;
    mat[NV-1][i1]=Uc*qhat/NV;
    mat[i1][NV] = -chi[i1];
    eps += abs(chi[i1]);
    //cout << k << scientific << " chi= "<< chi[k] << " mat= "<< mat[k][k] << endl;
  }

  // fix time by Newton method.
  for (int k=0; k<NV; k++) {

    // Initialize maximum value and index for pivot
    //int i_max = k;
    //double v_max = mat[i_max][k];
    // find greater amplitude for pivot if any
    //for (int i = k+1; i < NV; i++)
      //if (abs(mat[i][k]) > v_max) v_max = mat[i][k], i_max = i;

    //if (!mat[k][i_max]) {
    if (!mat[k][k]) {
      cout << "TimeConstraints::makeConstraints2 singlular matirx "<< mat[k][k] << " at k= "<< k
       	<< " nc= "<< NV << " tau= "<< tau << endl;
      //for(int k1=0;k1<NV;k1++) cout << k1 << " matkk= "<< mat[k1][k1] << " chi= "<< chi[k1] <<endl;
      auto pa1= particles.begin();
      for(int i=0;i<k;i++) pa1++;
      Vec4 r1=(*pa1)->getR();
      Vec4 p1=(*pa1)->getP();
	i1=0;
      for(auto pa2=particles.begin(); pa2 !=particles.end(); ++pa2,++i1) {
        Vec4 r2=(*pa2)->getR();
        Vec4 p2=(*pa2)->getP();
        Vec4 u12=p1+p2;
        u12 /= u12.mCalc();
        Vec4 r12=r1-r2;
      double ur12=u12*r12;
      double qt12 = (r12*r12 - ur12*ur12);
	cout << " mat= " << i1 << scientific << " " << mat[k][i1]
	  << " qt12= "<< qt12
	  << " G12= " << exp(qt12/(4*cWidth))*4*cWidth/qt12
	  << " t1= "<< r1[0]
	  << " t2= "<< r2[0]
	      << endl;
      }
      //mat[k][k]=1.0;
      //exit(1);
      return 0.0;
    }
   // Swap the greatest value row with current row.
   //if (i_max != k) swap_row(mat,NV, k, i_max);

    for (int i=k+1; i<NV; i++) {
      double f = mat[i][k]/mat[k][k];
      for (int j=k+1; j<=NV; j++) mat[i][j] -= mat[k][j]*f;
      mat[i][k] = 0;
    }
  }
  // Start calculating from last equation up to the first.
  for (int i = NV-1; i >= 0; i--) {
    for (int j=i+1; j<NV; j++) {
      mat[i][NV] -= mat[i][j]*mat[j][NV];
    }
    mat[i][NV] /= mat[i][i];
  }

  if(eps/NV>10.0) return eps/NV;

  i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    Vec4 r = (*pa)->getR();
    Vec4 rhat = r/r.pAbs();
    r += mat[i1][NV]*rhat;
    (*pa)->addR(r);
    //cout << " dt= "<< mat[i1][NV]<<endl;
  }

  return eps/NV;
}


void TimeConstraints::solveConstraints(double tau)
{
  int NV=particles.size();
  if(NV ==1) {
    double m=particles.front()->getEffectiveMass();
    if(m==0.0) m=1.0;
    particles.front()->lambda(1.0/m);
    return;
  } else if(NV==2) {
    EventParticle *p1 = particles.front();
    EventParticle *p2 = particles.back();
    Vec4 u=p1->getP() + p2->getP();
    u /= u.mCalc();
    p1->lambda( 1.0/(u*p1->getP()) );
    p2->lambda( 1.0/(u*p2->getP()) );
    return;
  }

  //chi.assign(NV,0.0);
  //vector<std::vector<double> > mat(NV,vector<double>(NV+1,0.0));
  mat.assign(NV,vector<double>(NV+1,0.0));

  Vec4 R=0.0;
  auto pend=--particles.end();
  int i1=0;
  for(auto pa1=particles.begin(); pa1 !=particles.end(); ++pa1,++i1) {
    Vec4 r1=(*pa1)->getR();
    Vec4 p1=(*pa1)->getP();
    R += r1;
    mat[i1][i1] += aSmall*2*(pHat*p1);
    int i2=NV-1;
    for(auto pa2=pend; pa2 !=pa1; --pa2,--i2) {
      //if(chi[i2]>1.0) continue;
      Vec4 r2=(*pa2)->getR();
      Vec4 p2=(*pa2)->getP();
      Vec4 u12=p1+p2;
      u12 /= u12.mCalc();
      Vec4 r12=r1-r2;
      double q12=r12*r12;
      //if(q12 > distMinSq) continue;
      if(q12 > 0.0) continue;
      double ur12=u12*r12;
      double G12=1.0;
      double dq1=2*p1*u12;
      double dq2=2*p2*u12;
      double qt12 = (q12 - ur12*ur12)/(4*cWidth);
      Vec4 uij = u12;
      q12 /= (4*cWidth);
      if(q12 > 10.0) continue;
      if(optGij==1) {
        G12= exp(q12)/abs(q12-epsG);
        dq1=G12*(2/(4*cWidth)*ur12*(1.-1./(q12-epsG))*2*(r12*p1) + 2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-1./(q12-epsG))*2*(r12*p2) + 2*(p2*uij));
      } else if(optGij==2) {
        G12= exp(q12)/(q12-epsG);
        dq1=G12*(2/(4*cWidth)*ur12*(1.-1./(q12-epsG))*2*(r12*p1) + 2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-1./(q12-epsG))*2*(r12*p2) + 2*(p2*uij));
      } else if(optGij==3) {
        G12= exp(q12);
        dq1=G12*(2/(4*cWidth)*ur12*2*(r12*p1) + 2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*2*(r12*p2) + 2*(p2*uij));
      } else if(optGij==4 || optGij==5) {
        G12= optGij==1 ? exp(qt12)/(qt12-epsG): exp(qt12)/abs(qt12-epsG);
        Vec4 q = r12 - ur12*u12;
        dq1=G12*(2/(4*cWidth)*ur12*(1.-1./(qt12-epsG))*2*(q*p1) + 2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-1./(qt12-epsG))*2*(q*p2) + 2*(p2*uij));
      } else if(optGij==6) {
        G12= exp(qt12);
        Vec4 q = r12 - ur12*u12;
        dq1=G12*(2/(4*cWidth)*ur12*2*(p1*q)+2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*2*(p2*q)+2*(p2*uij));
      }
      //chi[i1]  += G12*urij;
      //chi[i2]  -= G12*urij;

      // for Newton method.
      mat[i1][i1] += dq1;
      mat[i2][i2] += dq2;
      mat[i1][i2] = -dq2;
      mat[i2][i1] = -dq1;
      if(isnan(dq1) || isnan(dq2)) {
	cout << "dq1 = "<< dq1 << " dq2= "<< dq2 <<endl;
	cout << "G12= "<< G12 << " q12= "<< q12 <<endl;
	cout << "r1= " << r1;
	cout << "r2= " << r2;
	exit(1);
      }
    }
  }
  Vec4 Uc = optSeparable == 1 ? pHat : pHatC;
  //double eps=0.0;
  i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    if(abs(mat[i1][i1])<1e-15) mat[i1][i1]=1.0;
    mat[NV-1][i1]=2*((*pa)->getP()*Uc)/NV;
    //eps += abs(chi[i1]);
    //cout << i1 << " sovle "<< scientific << " chi= "<< chi[i1] << endl;
  }

  //chi[NV-1]=(Uc*R)/NV - tau;
  if(aSmall) for(int i=0;i<NV;i++) mat[NV-1][i]= 1.0;
  mat[NV-1][NV]= 1.0;

  // fix time by Newton method.
  for (int k=0; k<NV; k++) {
    if (!mat[k][k]) {
      cout << "TimeConstraints::solveConstraints singlular matirx "<< mat[k][k]  <<" at k= "<< k << endl;
      cout << " tau= "<< tau << " NV= "<< NV <<endl;
      i1=0;
      //for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
      //cout << i1 << " mat= "<< mat[i1][i1] << " lambda= "<< (*pa)->lambda() <<endl;}
      mat[k][k]=1.0;
    }
    for (int i=k+1; i<NV; i++) {
      double f = mat[i][k]/mat[k][k];
      for (int j=k+1; j<=NV; j++) mat[i][j] -= mat[k][j]*f;
      mat[i][k] = 0;
    }
  }
  // Start calculating from last equation up to the first.
  for (int i = NV-1; i >= 0; i--) {
    for (int j=i+1; j<NV; j++) {
      mat[i][NV] -= mat[i][j]*mat[j][NV];
    }
    mat[i][NV] /= mat[i][i];
  }

  i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    (*pa)->lambda(2.0*mat[i1][NV]);

    if(mat[i1][NV]<=0.0) {
      cout <<"TimeConstraints lambda<0? Backward propagation? "<< mat[i1][NV]<<endl;
    //(*pa)->lambda(1.0/(pHat*(*pa)->getP()));
    double m = (*pa)->getMass() !=0.0 ? (*pa)->getMass() : 1.0;
    (*pa)->lambda(1.0/m);
    //(*pa)->lambda(abs(2.0*mat[i1][NV]));
    }
    //float inf = std::numeric_limits<float>::infinity();
    if(isinf((*pa)->lambda())) {
      cout <<"TimeConstraints lambda inf? "<< mat[i1][NV] << " id= "<< (*pa)->getID() <<endl;
      exit(1);
    }
      
    if(isnan((*pa)->lambda())) {
      cout << "lambda= "<< (*pa)->lambda()<<endl;
      exit(1);
    }

    //cout << " lambda= "<< (*pa)->lambda() << " tau= "<< (*pa)->tevol()
    //   << " t= "<< (*pa)->getT() << " z= "<< (*pa)->getZ()
    //   << " vz= "<< (*pa)->getPz()*2.0*mat[i1][NV]
    //  <<endl;
  }
  //cout << "SolveConst eps= "<< eps << " nc= "<< particles.size() <<endl;
  //cin.get();

  return;
}

} // end of namespace jam2
