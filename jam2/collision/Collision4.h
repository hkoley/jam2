#ifndef jam2_collision_Collision4_h
#define jam2_collision_Collision4_h

#include <list>
#include <vector>
#include <set>
#include <fstream>
#include <jam2/collision/EventParticle.h>
#include <jam2/collision/TimeOrder.h>
#include <jam2/collision/InterList.h>
#include <jam2/xsection/CrossSection.h>
#include <Pythia8/Settings.h>
#include <jam2/collision/Collision.h>
#include <jam2/collision/CascBox.h>
#include <jam2/collision/CascCell.h>

namespace jam2 {

typedef std::list<EventParticle*>::iterator EventParIt;

class Collision4 : public Collision
{
private:
  CascCell* cascCell;
  std::vector<CascBox*> cascBox;
  std::vector<InterList*> interList;
  int optBoxBoundary,optColl;
  double dtNow, volNow,facSig;

public:
    Collision4(Pythia8::Settings* s,JamParticleData* jp, CrossSection* in,Pythia8::Rndm* r);
    ~Collision4();
  void clear();
  void init(const InitialCondition* initc);
    void makeCollisionList(double itime,double gtime);
    InterList* findNextCollision();
    void collisionUpdate();
    void collisionUpdate(InterList* inter);
    void collisionUpdate(vector<EventParticle*>& outgoing,double itime,double gtime);
    void cancelCollision(InterList* inter);
  bool checkNextCollisionTime(TwoBodyInterList* inter,double dtau1,double dtau2,
                 bool preHadronA,bool preHadronB);
  void searchCollisionPair(CascBox* box);
  void searchCollisionPair(EventParticle* p1, CascBox* box);

  void propagate(double ctime,int opt,int step=2);
  //TwoBodyInterList* hit2(EventParticle* i1, EventParticle* i2);
  bool doPauliBlocking(InterList* inter,vector<EventParticle*>& outgoing,int opt);
  void removeInteraction(EventParticle* i1);
  bool hit22(EventParticle* i1, EventParticle* i2);

private:
  void wallCollisionTime(EventParticle* p,int opt);
  void wallCollision(EventParticle* p);
  void removeInteraction2(InterList* inter);

};
}
#endif
