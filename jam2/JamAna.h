// Copyright (C) 2020 Yasushi Nara.

#ifndef jam2_JamAna_H
#define jam2_JamAna_H

#include <jam2/collision/EventParticle.h>
#include <jam2/collision/Collision.h>
#include <jam2/collision/InterList.h>
#include <Pythia8/Settings.h>
#include <string>
#include <vector>

namespace jam2 {

//using namespace Pythia8;

class CollisionHistory;
class AnaTimeDepParticle;
class AnaTimeDepFlow;
class AnaTimeDepDensity;
class AnaMeanField;
class AnaOutPutPhaseSpace;
class AnaTimeDepEt;

class JAMAna
{
private:
  Pythia8::Settings *settings;
  Collision *event;
  CollisionHistory *anaColl=0;
  AnaTimeDepParticle   *anaParticle=0;
  AnaTimeDepFlow   *anaFlow=0;
  AnaTimeDepDensity  *anaDens=0;
  AnaMeanField *anaPot=0;
  AnaOutPutPhaseSpace *anaPhase=0;
  AnaTimeDepEt *anaEt=0;
  int printFreq, printFreqPhaseSpace,nStep,overSample;

public:
  JAMAna(Pythia8::Settings *s,Collision* ev): settings(s),event(ev) {}
  void initTimeDependentAna(const double ecm, const double gamcm,const double ycm);
  void eventInitTimeDependentAna();
  void anaTimeDependent(const int step, const double timeNow, const int iEvent);
  void statTimeDependentAna(const double coltime,InterList* inter,vector<EventParticle*> outgoing);
  void printTimeDependentAna(const double t, const int nev);
  void finTimeDependentAna(const int nev);
};

class CollisionHistory
{
private:
  int    nS;
  double dS, sMax,sMin;
  std::vector<double> scollBB, scollMB, scollMM, scollBBar, scollBarBar;
  std::vector<double> scollBBc, scollMBc, scollMMc, scollBBarc, scollBarBarc;
  std::vector<double> scollPP;
  double dt,gamCM; // time slice for time dependent analysis.
  int mprint; // number of time step
  std::vector<int> BBcoll, MBcoll, MMcoll;
  std::vector<int> BBcolly, MBcolly, MMcolly;
  std::vector<int> BBy,BBf,MBy,MBf;
  std::vector<int> BBStry,BBStrf,MBStry,MBStrf,DecStry,DecStrf;
public:
  CollisionHistory(double ecm,double ftime,double dta,double gam);
  void fill(InterList* inter,const std::vector<EventParticle*>& outgoing);
  void print(int nev,std::string outFile);
};

class AnaTimeDepParticle
{
private:
  double dt; // time slice for time dependent analysis.
  int mprint; // number of time step
  int nPrint;
  int nP; // number of particles
  double gamCM,yCut;
  std::vector<std::vector<double> > npart;
  std::vector<std::vector<int> > mult;
public:
  AnaTimeDepParticle(double ftime, double dta,double gam, double ycut);
  void fill(double ctime,std::list<EventParticle*>& plist);
  void ana(int itime, double ct,std::list<EventParticle*>& plist);
  void print(std::string outfile,const int nevent);
  void init() {nPrint=0;}
};

class AnaTimeDepFlow
{
private:
  double dt; // time slice for time dependent analysis.
  int mprint; // number of time step
  int nPrint;
  int nP; // number of particles
  double gamCM,yCut,yCutF,yCutMax;
  std::vector<std::vector<double> > v1,v2, v1y,v2y, v1r,v2r,v1neg,v1pos;
  std::vector<std::vector<int> > mult,multy,multr;
  std::vector<double> xave,xavey;
  std::vector<int> xmult,xmulty;
  std::vector<double> ecc2,ecc2y;

  // flow at foward and backward direction.
  std::vector<std::vector<double> > v1f,v2f;
  std::vector<std::vector<int> > multf;

  std::vector<double > v1Lambda,v1LambdaY,v1LambdaF;
  std::vector<double > v2Lambda,v2LambdaY,v2LambdaF;
  std::vector<int> multLambda,multLambdaY,multLambdaF;

public:
  AnaTimeDepFlow(double ftime, double dta,double gam, double ycut,double yf=1.0,double ymax=3.0);
  void fill(double ctime,std::list<EventParticle*>& plist);
  void ana(int itime, double ct,std::list<EventParticle*>& plist);
  void print(std::string outfile);
  void init() {nPrint=0;}
};

class AnaTimeDepDensity
{
private:
  double dt; // time slice for time dependent analysis.
  int mprint; // number of time step
  int nPrint;
  int nP; // number of particles
  double wG,facG,gamCM,yCut;
  std::vector<double> rho1,rho2,rhos1,rhos2;
  std::vector<std::vector<Pythia8::Vec4> > JB, JB2, JB3;
public:
  AnaTimeDepDensity(double ftime, double dta,double g, double ycut);
  void fill(double ctime,std::list<EventParticle*>& plist);
  void ana(int itime, double ct,std::list<EventParticle*>& plist);
  void print(std::string outfile,int nevent);
  void init() {nPrint=0;}
};

class AnaOutPutPhaseSpace
{
private:
  std::string dir;
  double dt; // time slice for time dependent analysis.
  int mprint; // number of time step
  int nPrint;
  int nP; // number of particles
  double wG,facG;
public:
  AnaOutPutPhaseSpace(double ftime, double dta);
  void fill(int iev,double ctime,std::list<EventParticle*>& plist);
  void ana(int iev,int itime, double ct,std::list<EventParticle*>& plist);
  void init() {nPrint=0;}
};

class AnaOutPutDensity
{
private:
  std::string dir;
  double dt; // time slice for time dependent analysis.
  int mprint; // number of time step
  int nPrint;
  int nP; // number of particles
  double wG,facG;
  std::vector<std::vector<double> > j0,jx,jy,jz;
public:
  AnaOutPutDensity(double ftime, double dta);
  void ana(int iev,int itime, double ct,std::list<EventParticle*>& plist);
  void init() {nPrint=0;}
};

class AnaMeanField
{
private:
  double dt; // time slice for time dependent analysis.
  int mprint; // number of time step
  int nPrint;
  int nP; // number of particles
  double gamCM,yCut;
  std::vector<double>  rhoS,rhoB,potSk,potSm;
  std::vector<Pythia8::Vec4> potVk,potVm;
public:
  AnaMeanField(double ftime, double dta,double gam, double ycut);
  void fill(double coltime,std::list<EventParticle*>& plist);
  void print(std::string outfile,int nevent);
  void init() {nPrint=0;}
};

class ParticleDensity
{
private:
  Pythia8::Settings *settings;
  //Fluid* fluid;
  double rho,rhob,einv;
  int optGauss;
  double widG,widG2,widCof,gVolume;
  double dX,dY,dZ;
  int optPropagate,optDens,optPreHadron;
public:
  //ParticleDensity(Pythia8::Settings* s, Fluid* f);
  ParticleDensity(Pythia8::Settings* s);
  double getRho() {return rho;}
  double getRhob() {return rhob;}
  double getEdens() {return einv;}
  int  computeEnergyMomentum(std::list<EventParticle*>& plist,EventParticle* i1,
	double ctime,int iopt);
  double gaussSmear(const Vec4& pv, const Vec4& dr);

};

class AnaTimeDepEt
{
private:
  Pythia8::Settings *settings;
  double dt; // time slice for time dependent analysis.
  double U0,Uz,dE;
  int mprint; // number of time step
  int nPrint;
  double yCut,transverseRad,trArea,etaCM;
  double vBox;
  int withBox,nStep,overSample;
  std::vector<double> Et,dens,edens,coll,detaColl,pT,pZ;
  std::vector<int> dN,mult,cmult;
  Pythia8::Hist dNdE,dNdpt,dNdpz;
  bool collpair=false;
  std::ofstream ofs1,ofs2;
public:
  AnaTimeDepEt(Pythia8::Settings* s,double ftime, double dta, double ycut,double eta,int box,int nstep,int no);
  ~AnaTimeDepEt();
  void fill(InterList* inter,std::vector<EventParticle*>& outgoing);
  void fill(double coltime,std::list<EventParticle*>& plist);
  void fillp(EventParticle* i, int itime,double t);
  void ana(int itime, double ct,std::list<EventParticle*>& plist);
  void print(std::string outfile,int nev);
  void init() {nPrint=0;}
};

}
#endif // jam2_JamAna_H


